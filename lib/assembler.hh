#ifndef DUNE_PY_PDELAB_ASSEMBLER_HH
#define DUNE_PY_PDELAB_ASSEMBLER_HH

#include <dune/pdelab/finiteelementmap/p0fem.hh>
#include <dune/pdelab/finiteelementmap/qkfem.hh>
#include <dune/pdelab/finiteelementmap/qkdg.hh>
#include <dune/pdelab/constraints/conforming.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include <dune/pdelab/gridfunctionspace/interpolate.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/pdelab/backend/numpy.hh>

#include <boost/python.hpp>
namespace py = boost::python;

#include "pygrid.hh"

enum SpaceType { Conforming, Discontinuous };

template<typename GV, typename FEM, typename P>
struct PoissonSetup;

/* Helper struct to */
template<typename GV, typename DF, SpaceType T>
struct FEM
{
    typedef Dune::PDELab::QkLocalFiniteElementMap<GV,DF,double,1> type;
    static
    type instance(const GV & gv) { return type(gv); }
};

template<typename GV, typename DF>
struct FEM<GV,DF,Discontinuous>
{
    typedef Dune::PDELab::QkDGLocalFiniteElementMap<DF,double,1,GV::dimension> type;
    static
    type instance(const GV & gv) { return type(); }
};

template<typename GV, SpaceType ElementWise, typename Problem>
py::object generate_rhs (const GridView<GV> & gv_wrapped, const Problem & problem)
{
    // using GV = typename WrappedGV::Impl;
    GV gv = gv_wrapped.impl();

    // make finite element map
    typedef typename GV::Grid::ctype DF;
    typedef typename FEM<GV,DF,ElementWise>::type FEM;
    FEM fem = ::FEM<GV,DF,ElementWise>::instance(gv);

    // make matrix setup descrition
    typedef PoissonSetup<GV, FEM, Problem> Setup;
    Setup setup;

    // constants and types
    typedef typename FEM::Traits::FiniteElementType::Traits::
        LocalBasisType::Traits::RangeFieldType R;
    typedef Dune::PDELab::ConformingDirichletConstraints CON;

    // make function space
    typedef Dune::PDELab::GridFunctionSpace<
        GV,
        FEM,
        CON,
        Dune::PDELab::NumPyVectorBackend
        > GFS;
    GFS gfs(gv,fem);
    gfs.name("solution");
    gfs.update();

    // make local operator
    typedef typename Setup::LocalOperator LOP;
    LOP lop = setup.lop();

    // make constraints map and initialize it from a function
    typedef typename GFS::template ConstraintsContainer<R>::Type C;
    C cg;
    cg.clear();
    Dune::PDELab::constraints(setup.constraints_parameters(),gfs,cg);

    // make grid operator
    typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,
                                       Dune::PDELab::NumPyMatrixBackend,
                                       double,double,double,
                                       C,C> GridOperator;
    GridOperator gridoperator(gfs,cg,gfs,cg,lop);

    // make coefficent Vector and initialize it from a function
    typedef typename GridOperator::Traits::Domain DV;
    DV x0(gfs);
    Dune::PDELab::interpolate(setup.dirichlet_extension(gv),gfs,x0);
    Dune::PDELab::set_nonconstrained_dofs(cg,0.0,x0);

    // evaluate residual w.r.t initial guess
    typedef typename GridOperator::Traits::Range RV;
    RV r(gfs);
    r = 0.0;
    gridoperator.residual(x0,r);

    // return tuple of matrix and vector
    return Dune::PDELab::NumPy::getPyObject(r);
}

template<typename GV, SpaceType ElementWise, template<typename,typename,typename> class TSetup, typename Problem>
py::object generate_matrix (const GridView<GV> & gv_wrapped, const Problem & problem)
{
    GV gv = gv_wrapped.impl();

    // make finite element map
    typedef typename GV::Grid::ctype DF;
    typedef typename FEM<GV,DF,ElementWise>::type FEM;
    FEM fem = ::FEM<GV,DF,ElementWise>::instance(gv);

    // make matrix setup descrition
    typedef TSetup<GV,FEM,Problem> Setup;
    Setup setup;

    // constants and types
    typedef typename FEM::Traits::FiniteElementType::Traits::
        LocalBasisType::Traits::RangeFieldType R;

    // make function space
    typedef Dune::PDELab::GridFunctionSpace<
        GV,
        FEM,
        typename Setup::Constraints,
        Dune::PDELab::NumPyVectorBackend
        > GFS;
    GFS gfs(gv,fem);
    gfs.name("solution");
    gfs.update();

    // make local operator
    typedef typename Setup::LocalOperator LOP;
    LOP lop = setup.lop();

    // make constraints map and initialize it from a function
    typedef typename GFS::template ConstraintsContainer<R>::Type C;
    C cg;
    cg.clear();
    Dune::PDELab::constraints(setup.constraints_parameters(),gfs,cg);

    // make grid operator
    typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,
                                       Dune::PDELab::NumPyMatrixBackend,
                                       double,double,double,
                                       C,C> GridOperator;
    GridOperator gridoperator(gfs,cg,gfs,cg,lop);

    // make coefficent Vector and initialize it from a function
    typedef typename GridOperator::Traits::Domain DV;
    DV x0(gfs,0.0);
    Dune::PDELab::interpolate(setup.dirichlet_extension(gv),gfs,x0);
    Dune::PDELab::set_nonconstrained_dofs(cg,0.0,x0);

    // represent operator as a matrix
    typedef typename GridOperator::Traits::Jacobian M;
    M m(gridoperator);
    gridoperator.jacobian(x0,m);

    // return matrix
    return Dune::PDELab::NumPy::getPyObject(m);
}

#endif // DUNE_PY_PDELAB_ASSEMBLER_HH
