// elliptic example problem for the paper
//
// isotropic tensor with micro structure
struct Problem17
{
    static const bool has_exact_solution = false;
    static const bool pure_neumann = false;
    static const std::string name() { return "Problem17"; }

    template<typename GV, typename RF>
    class Parameters
    {
        typedef Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;

    public:
        typedef Dune::PDELab::ConvectionDiffusionParameterTraits<GV,RF> Traits;

        //! tensor diffusion coefficient
        typename Traits::PermTensorType
        A (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
        {
            typename Traits::DomainType x = e.geometry().global(xlocal);

            double eps = constants_epsilon;
            double floor_x_0 = std::floor( x[0] + x[1] );
            double floor_x_1 = std::floor( x[1] - x[0] );
            RF c = 1+1e-8 + (0.5 * sin( floor_x_0 + std::floor( x[0] / eps) + std::floor(x[1] / eps)) )
              + (0.5 * cos( floor_x_1 + std::floor( x[0] / eps) + std::floor(x[1] / eps) ) );
            return {{c, 0.0},{0.0, c}};
            // paraview:
            // 1+1e-8 + (0.5 * sin( floor(coordsX+coordsY) + floor( coordsX / .031250) + floor(coordsX/ .031250) )) + (0.5 * cos( floor(coordsY-coordsX)+ floor( coordsX / .031250) + floor(coordsY / .031250) ) )

        }

        //! velocity field
        typename Traits::RangeType
        b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
        {
            typename Traits::RangeType v(0.0);
            return v;
        }

        //! sink term
        typename Traits::RangeFieldType
        c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
        {
            return 0.0;
        }

        //! source term
        typename Traits::RangeFieldType
        f (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
        {
            return -1;
        }

        //! boundary condition type function
        BCType
        bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
        {
            return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
        }

        //! Dirichlet boundary condition value
        typename Traits::RangeFieldType
        g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
        {
            return 0.0;
        }

        //! Neumann boundary condition
        typename Traits::RangeFieldType
        j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
        {
            assert(false && "wrong boundary condition");
            return 0.0;
        }

        //! outflow boundary condition
        typename Traits::RangeFieldType
        o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
        {
            assert(false && "wrong boundary condition");
            return 0.0;
        }
    };

}; // end Problem17
