#!/usr/bin/env python

import sys
import os
import site
site.addsitedir(os.path.dirname(sys.argv[0])+"/..")

import math
import warnings

# initialize MPI (mayhaps we link DUNE against MPI?!)
from mpi4py import MPI

# import numpy/scipy
import numpy as np
import math
import scipy.sparse as sparse

# import DUNE
import pypdelab as pde
import lod
from lod import restrictions
from lod import timefunc
from lod import utils

# import matplotlib
import pandas as pd
import matplotlib
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
plotdata = pd.DataFrame(columns=list(['L','l','H','h','overlap']))

@timefunc
def run_test(basename,N,L,refine,level,overlap,write_vtk,model,verbose):
    global plotdata
    overlap = overlap * 1.0
    overlapsize = int((1<<(refine-level))*overlap)    # overlap for patch
    name = "%s-%i-%i-%i" % (basename,refine,level,overlap*10)
    # prepend the output to easily see which simulation we are running...
    annotation = utils.prependstream(name+":\t", "sys.stdout", globals())
    # create grid and grid views
    H = (math.pow(0.5, level)*np.array(L)/np.array(N))
    # store some configuration options
    plotdata.loc[name,'L'] = level
    plotdata.loc[name,'l'] = refine
    plotdata.loc[name,'H'] = H[0]
    plotdata.loc[name,'h'] = math.pow(0.5, refine)*L[0]/N[0]
    plotdata.loc[name,'overlap'] = overlap
    # create grid and grid views
    print "create Grid"
    grid = pde.Grid(L,N)
    for i in range(refine):
        grid.globalRefine()
    gv = grid.leafGridView()
    coarse_gv = grid.levelGridView(level)
    dim = coarse_gv.dimension()
    print "generate local to global map"
    Sigma = timefunc(pde.generate_local_global_map)(gv)
    # generate sub domain info
    print "Mesh width:", H
    coarse_positions = coarse_gv.cellpositions()
    coarse_positions = coarse_positions.reshape((coarse_positions.shape[0]/dim,dim))
    fine_positions = gv.cellpositions()
    fine_positions = fine_positions.reshape((fine_positions.shape[0]/dim,dim))
    N_patches = coarse_positions.shape[0]
    print "generate fine mesh Dirichlet boundary information"
    dirichletindicator = pde.generate_dirichletinformation(gv,model)
    N_dofs = len(dirichletindicator)
    print "generate cell/cell info"
    coarse_cell_map = pde.generate_cell_cell_info(grid,level)
    ######################################################
    # get coarse-to-fine basis mapping in coordinate format
    print "generate basis functions projection"
    val, row, col = pde.generate_basis_function_projection(grid,level)
    Psi = sparse.coo_matrix((val, (row, col)))
    val, row, col = (None, None, None)
    ######################################################
    # generate subdomain info
    SubInfo = restrictions.generate_subdomain_info(grid,level,dirichletindicator,Sigma,H,overlap,Psi)
    # generate matrices
    print "generate element wise stiffness matrix and rhs"
    AE = timefunc(pde.generate_element_stiffnessmatrix)(gv,model)
    bE = timefunc(pde.generate_element_rhs)(gv,model)
    print "generate element wise massmatrix"
    ME = timefunc(pde.generate_element_massmatrix)(gv)
    print "generate global matrizes"
    M = Sigma * ME * Sigma.transpose()
    A = Sigma * AE * Sigma.transpose()
    ######################################################
    warnings.warn('fix dirichlet BC handling')
    print "generate fine mesh Dirichlet boundary corrections"
    b = Sigma * bE
    bh = np.logical_not(pde.generate_dirichletinformation(gv,model))
    Bh = utils.diagonal_matrix(bh)
    # lagrange interpolation operator from fine space into coarse space
    print "generate Lagrange restriction operator"
    Lag = (Psi == 1.0) * 1.0
    print "generate coarse mesh Dirichlet boundary corrections"
    BH = Lag * Bh * Lag.transpose()
    ######################################################
    # construct coarse space correction
    print "generate corrector functions"
    timefunc.reset()
    (Q_Psi,W) = lod.compute_correction(AE,bE,BH,M,Psi,Sigma,SubInfo,verbose)
    plotdata.loc[name,'t_corr'] = timefunc.accumlated
    # restrict to coarse space
    P = Psi + Q_Psi
    # create sparse solver
    solve = timefunc(lod.spsolve)
    # setup vtk writer
    vtk = pde.VtkWriter(grid.leafGridView(),0)
    # solve full problem
    print "Solve full problem"
    timefunc.reset()
    x_h = solve(A,b,Bh)
    plotdata.loc[name,'t_full'] = timefunc.accumlated
    if write_vtk > 0:
        vtk.addVertexData(x_h,"u_full")
    # solve coarse problem
    print "Solve coarse problem"
    timefunc.reset()
    x = Psi.transpose() * solve(Psi*A*Psi.transpose(),Psi*b,BH)
    plotdata.loc[name,'t_coarse'] = timefunc.accumlated
    err = x-x_h
    print "error(u_coarse) = %e" % math.sqrt(np.dot(err,(M*err)))
    print "error_rel(u_coarse) = %e" % (math.sqrt(np.dot(err,(M*err)))/math.sqrt(np.dot(x_h,(M*x_h))))
    plotdata.loc[name,'err_coarse'] = math.sqrt(np.dot(err,(M*err)))/math.sqrt(np.dot(x_h,(M*x_h)))
    if write_vtk > 0:
        vtk.addVertexData(x,"u_coarse")
        vtk.addVertexData(err,"err_coarse")
    # solve LOD problem
    # print "Solve LOD problem"
    # x = P.transpose()*solve(P*A*P.transpose(),P*(b),BH)
    # if write_vtk > 0:
    #     vtk.addVertexData(x,"u_lod_nc")
    print "Solve corrected LOD problem"
    timefunc.reset()
    x = P.transpose()*solve(P*A*P.transpose(),P*(b-A*W),BH) + W
    plotdata.loc[name,'t_LOD'] = timefunc.accumlated
    err = x-x_h
    print "error(u_lod) = %e" % math.sqrt(np.dot(err,(M*err)))
    print "error_rel(u_lod) = %e" % (math.sqrt(np.dot(err,(M*err)))/math.sqrt(np.dot(x_h,(M*x_h))))
    plotdata.loc[name,'err_LOD'] = math.sqrt(np.dot(err,(M*err)))/math.sqrt(np.dot(x_h,(M*x_h)))
    if write_vtk > 0:
        vtk.addVertexData(x,"u_lod_c")
        vtk.addVertexData(err,"err_lod")
    if write_vtk > 0:
        print ">>> Write VTK output %s.vtu" % name
        vtk.write(name)
    print plotdata
    # we modified sys.stdout, bring it back to default state
    annotation.detach()

def generate_plots(basename, fine_level, show = False):
    if not show:
        print "do not show output"
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    # from mpltools import annotation
    f = plt.figure()
    name = "%s-%i" % (basename,fine_level)
    refinedata = plotdata
    refinedata['t_lod_all'] = refinedata['t_corr'] + refinedata['t_LOD']
    X = np.arange(2, fine_level, 1)
    T = np.array([4.3,4.9,4.9,4.3])
    Tx = np.array([4.3,4.3,4.9,4.3])
    T2 = np.array([3.1,3.8,3.8,3.1])
    T2x = np.array([3.1,3.1,3.8,3.1])
    T3 = np.array([4.4,5.6,5.6,4.4])
    T3x = np.array([4.4,4.4,5.6,4.4])
    # T2 = np.array([2.3,4.8,4.8,2.3])
    # T2x = np.array([2.3,2.3,4.8,2.3])
    h1slope = np.power(2.,-Tx*1)
    h2slope = np.power(2.,-Tx*2)
    h3slope = np.power(2.,-Tx*3)
    h4slope = np.power(2.,-Tx*4)
    h5slope = np.power(2.,-Tx*5)
    h6slope = np.power(2.,-Tx*6)
    h7slope = np.power(2.,-T3x*7)
    h8slope = np.power(2.,-Tx*8)
    hm1slope = np.power(2.,T2x*1)
    hm2slope = np.power(2.,T2x*1.5)
    hm3slope = np.power(2.,T2x*3)
    ## plot 1
    f = plt.figure()
    ax1 = refinedata.plot(x='L',y='err_coarse_p',label='$err_{coarse,H}$',color='#FF7F0E',marker='.',ax=f.gca(),legend=True)
    refinedata.plot(x='L',y='err_LOD_p',label='$err_{LOD,H}$',color='#1F77B4',marker='.',ax=ax1,legend=True)
    ax2 = refinedata.plot(x='L',y='t_coarse',label='$t_{coarse,H}$',color='#FF7F0E',marker='x',ls='--',ax=ax1,secondary_y=True,legend=True)
    refinedata.plot(x='L',y='t_LOD',label="$t_{LOD,H}$",color='#1F77B4',ax=ax1,marker='x',ls='--',secondary_y=True,legend=True)
    refinedata.plot(x='L',y='t_corr',label='$t_{corr}$',color='#D62728',marker='x',ls='--',ax=ax1,secondary_y=True,legend=True)
    refinedata.plot(x='L',y='t_full',label='$t_{fine}$',color='#2CA02C',marker='x',ls='--',ax=ax1,secondary_y=True,legend=True)
    # annotation.slope_marker((10, 2), 2, ax=ax1)
    ax1.set_ylabel('relative error [%]')
    ax1.get_xaxis().set_major_formatter(
        matplotlib.ticker.FuncFormatter(lambda x, p: "1/"+format(int(2**x),',')))
    ax1.set_xlabel('H')
    ax1.set_xticks(X)
    ax1.plot(list(T),list(4e1*h2slope),color="#1F77B4")
    ax1.text(4.7,1.1e-1,"$H^{2}$",color="#1F77B4")
    ax1.plot(list(T),list(7e3*h2slope),color="#FF7F0E")
    ax1.text(4.7,20,"$H^{2}$",color="#FF7F0E")
    ax2.plot(list(T2),list(2e-1*hm2slope),ls='--',color="#D62728")
    ax2.text(3.55,2.45,"$H^{-\\frac{3}{2}}$",color="#D62728")
    ax2.set_ylabel('computation time [s]')
    ax2.semilogy()
    ax1.semilogy()
    handles1, labels1 = ax1.get_legend_handles_labels()
    handles2, labels2 = ax2.get_legend_handles_labels()
    lgd1 = ax1.legend(handles = handles1, loc=2)
    lgd2 = ax2.legend(handles = handles2, loc=1)
    f.savefig("%s-refine.pdf" % name, bbox_extra_artists=(lgd1,lgd2,), bbox_inches='tight')
    ## plot 2
    f = plt.figure()
    ax1 = refinedata.plot(x='L',y='err_vs_time_coarse',label='${coarse,H}$',color='#FF7F0E',marker='.',ax=f.gca(),legend=True)
    refinedata.plot(x='L',y='err_vs_time_LOD',label='${LOD,H}$',marker='.',color='#1F77B4',ax=ax1,legend=True)
    ax1.set_ylabel('rel. error/time')
    ax1.semilogy()
    ax1.get_xaxis().set_major_formatter(
        matplotlib.ticker.FuncFormatter(lambda x, p: "1/"+format(int(2**x),',')))
    ax1.set_xlabel('H')
    ax1.set_xticks(X)
    ax1.plot(list(T3),list(3e7*np.power(2.,-T3x*6.5)),color="#1F77B4")
    ax1.text(5.65,0.25e-1,"$H^{6.5}$",color="#1F77B4")
    ax1.plot(list(T3),list(2.5e7*np.power(2.,-T3x*4)),color="#FF7F0E")
    ax1.text(5.65,19,"$H^{4}$",color="#FF7F0E")
    lgd = ax1.legend(loc=2)
    f.savefig("%s-performance.pdf" % name, bbox_extra_artists=(lgd,), bbox_inches='tight')
    if show:
        plt.show()

def convergencetest(basename,model,R):
    pvd = utils.PVDWriter()
    for l in range(2,R):
        o = 0.9*l
        run_test(basename,N=[1,1], L=[1,1], refine=R, level=l, overlap=o, model=model, write_vtk=1, verbose=0)
        vtk = "%s-%i-%i-%i.vtu" % (basename,R,l,o*10.0)
        pvd.addvtk(vtk)
    pvd.write("%s-ol:2-refine.pvd" % (basename))
            
def overlaptest(basename,model,R):
    pvd = utils.PVDWriter()
    l = 3
    for o in [0.1]+list(np.arange(0.5,3.1,0.5)):
        H = math.pow(0.5, l)
        run_test(basename,N=[1,1], L=[1,1], refine=R, level=l, overlap=o, model=model, write_vtk=1, verbose=0)
        vtk = "%s-%i-%i-%i.vtu" % (basename,R,l,o*10.0)
        pvd.addvtk(vtk)
    pvd.write("%s-l3-overlap.pvd" % (basename))
            
def main():
    global plotdata
    utils.print_dependencies()
    np.set_printoptions(threshold=np.inf)
    basename = "ellipticsource"
    fine_level = 7
    model = pde.Problem17()
    print "running test for ", model.name()
    timefunc.enable = True
    basename = basename + "-" + model.name()
    fname = "%s-%i.csv" % (basename,fine_level)
    if not os.path.isfile(fname):
        convergencetest(basename,model,fine_level)
        # overlaptest(basename,model)
        plotdata.to_csv(fname)
    else:
        plotdata = pd.read_csv(fname, index_col=0)
    plotdata.sort_values(by=['L','l','overlap'], inplace=True)
    plotdata['t_LOD_all'] = plotdata.t_LOD + plotdata.t_corr
    plotdata['err_LOD_p'] = plotdata.err_LOD * 100
    plotdata['err_coarse_p'] = plotdata.err_coarse * 100
    plotdata['speedup_LOD'] = plotdata.t_full / plotdata.t_LOD
    plotdata['speedup_coarse'] = plotdata.t_full / plotdata.t_coarse
    plotdata['err_vs_time_LOD'] = plotdata.err_LOD / plotdata.t_LOD
    plotdata['err_vs_time_coarse'] = plotdata.err_coarse / plotdata.t_coarse
    generate_plots(basename, fine_level, show=False)
    return

if __name__ == "__main__":
    np.set_printoptions(threshold=np.inf)
    main()
