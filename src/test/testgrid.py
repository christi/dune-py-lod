#!/usr/bin/python -u

import sys
import os
import site
site.addsitedir(os.path.dirname(sys.argv[0])+"/..")

import math
import warnings

# initialize MPI (mayhaps we link DUNE against MPI?!)
from mpi4py import MPI

# import numpy/scipy
import numpy as np
import scipy
import scipy.sparse as sparse
from scipy.sparse import linalg as splinalg

# import DUNE
import pypdelab as pde
import lod
from lod import timefunc

dim = 2

@timefunc
def run_test(N,h,refine,overlap):
    L = [h*l for l in N]
    grid = pde.Grid(L,N)
    for i in range(refine):
        grid.globalRefine()
    gv = grid.leafGridView()
    coarse = grid.levelGridView(1)

    pos = gv.cellpositions()
    pos = pos.reshape((pos.shape[0]/dim,dim))
    cpos = coarse.cellpositions()
    cpos = cpos.reshape((cpos.shape[0]/dim,dim))

    print type(pos)
    print pos
    print cpos

if __name__ == "__main__":
    run_test(N=[1,3], h=1.0, refine=2, overlap=0.5)
