#!/usr/bin/python

import sys
import os
import site
site.addsitedir(os.path.dirname(sys.argv[0])+"/..")

# initialize MPI (mayhaps we link DUNE against MPI?!)
from mpi4py import MPI
# import numpy/scipy
import numpy as np
import scipy.sparse as sparse
# import DUNE
import numpybackendtest as test

# generate (matrix,vector)
g = test.generate()
print type(g)
print type(g[0])
print type(g[1])
for v in g[1]:
    print v,
print
print "py two-norm ", np.linalg.norm(g[1],2)
print "py one-norm ", np.linalg.norm(g[1],1)
print "py inf-norm ", np.linalg.norm(g[1],np.inf)
print "py dot-prod ", g[1].dot(g[1])
