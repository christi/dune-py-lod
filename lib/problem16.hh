struct Problem16
{
    static const bool has_exact_solution = false;
    static const bool pure_neumann = true;
    static const std::string name() { return "Problem16"; }

    template<typename GV, typename RF>
    class Parameters
    {
        typedef Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;

    public:
        typedef Dune::PDELab::ConvectionDiffusionParameterTraits<GV,RF> Traits;

        //! tensor diffusion coefficient
        typename Traits::PermTensorType
        A (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
        {
            typename Traits::DomainType x = e.geometry().global(xlocal);

            const RF Pi = Dune::MathematicalConstants<RF>::pi();
            const RF A00 = 2.0 * ( 1.0 / (8.0 * Pi * Pi) ) * ( 1.0 / ( 2.0 + cos( 2.0 * Pi * (x[0] / constants_epsilon) ) ) );
            const RF A11 = ( 1.0 / (8.0 * Pi * Pi) ) * ( 1.0 + ( 0.5 * cos( 2.0 * Pi * (x[0] / constants_epsilon) ) ) );

            return {{A00, 0.0},{0.0, A11}};
        }

        //! velocity field
        typename Traits::RangeType
        b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
        {
            typename Traits::RangeType v(0.0);
            return v;
        }

        //! sink term
        typename Traits::RangeFieldType
        c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
        {
            return 0.0;
        }

        //! source term
        typename Traits::RangeFieldType
        __attribute__((hot))
        f (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
        {
            typename Traits::DomainType x = e.geometry().global(xlocal);

            return x[0]+x[1]-1.0;

        }

        //! boundary condition type function
        BCType
        bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
        {
            return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Neumann;
        }

        //! Dirichlet boundary condition value
        typename Traits::RangeFieldType
        g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
        {
            // assert(false && "wrong boundary condition");
            return 0.0;
        }

        //! Neumann boundary condition
        typename Traits::RangeFieldType
        j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
        {
            return 0.0;
        }

        //! outflow boundary condition
        typename Traits::RangeFieldType
        o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
        {
            assert(false && "wrong boundary condition");
            return 0.0;
        }
    };

}; // end Problem16
