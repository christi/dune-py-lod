import numpy as np
import scipy
import scipy.sparse as sparse
from scipy.sparse import linalg as splinalg
from pyamg import smoothed_aggregation_solver

import utils
import time

defaultsolvertype = "factorize+superlu"

def spsolve(A,b,BC,pure_neumann=None,solvertype=None):
    import inspect
    import sys
    it = 0
    def __sparse_inverse_operator(Mat,solvertype):
        if solvertype == "cg":
            return lambda b: splinalg.cg(Mat, b, tol=1e-20)[0]
        elif solvertype == "cg+amg":
            amg1 = time.time()
            amg = smoothed_aggregation_solver(Mat)
            amg2 = time.time()
            print "AMG setup ", (amg2-amg1), "sec"
            M = amg.aspreconditioner()
            return lambda b: splinalg.cg(Mat, b, tol=1e-20, M=M)[0]
        elif solvertype == "cg+ilu":
            ilu1 = time.time()
            ILU = splinalg.spilu(Mat, fill_factor=1.0)
            ilu2 = time.time()
            M=scipy.sparse.linalg.LinearOperator(Mat.shape,ILU.solve)
            print "ILU setup ", (ilu2-ilu1), "sec"
            return lambda b: splinalg.cg(Mat, b, tol=1e-20, M=M)[0]
        elif solvertype == "superlu":
            splinalg.use_solver(useUmfpack=False)
            return lambda b: splinalg.spsolve(Mat,b)
        elif solvertype == "umfpack":
            splinalg.use_solver(useUmfpack=True)
            return lambda b: splinalg.spsolve(Mat,b)
        else:
            raise "unknown solver type"
    BC = utils.eliminate_empty_rows(BC)
    if pure_neumann is None:
        pure_neumann = (BC.shape[0] == BC.shape[1])
    A = BC * A * BC.transpose()
    b = BC * b
    b = np.asarray(b)
    if pure_neumann:
        weight = np.min(np.abs(A.diagonal()))
        print "weight",weight
        _s = sparse.coo_matrix(np.ones(A.shape[0])*weight)
        _z = sparse.csr_matrix([0])
        A = sparse.vstack((sparse.hstack((A,_s.transpose())),sparse.hstack((_s,_z))))
        b = np.append(b,0)
        # b = sparse.vstack((np.matrix(b).transpose(),0))
    #### x = splinalg.spsolve(A,b)
    ####
    if solvertype is None:
        if A.shape[0] > 50000:
            solvertype = "cg+amg"
        else:
            solvertype = "superlu"
    print "Sparse solver:",A.shape,b.shape,"using",solvertype,pure_neumann
    # print np.linalg.cond(A.todense())
    x = __sparse_inverse_operator(A,solvertype)(b)
    if pure_neumann :
        x = x[:-1]
    return BC.transpose() * x

class InverseOperator(object):
    def __init__(self,A,dirichletindicator,solvertype = defaultsolvertype):
        # print "using linear solver ", solvertype
        self._solvertype = solvertype
        self._tol = 1e-20
        self._not_dirichlet = dirichletindicator
        self._pure_neumann = np.all(dirichletindicator)
        A = A[self._not_dirichlet,:][:,self._not_dirichlet]
        if self._pure_neumann:
            # print "\tpure Neumann setting"
            _s = sparse.coo_matrix(np.ones(A.shape[0]))
            _z = sparse.csr_matrix([0])
            A = sparse.vstack((sparse.hstack((A,_s.transpose())),sparse.hstack((_s,_z))))
        self._apply_inverse = self.__sparse_inverse_operator(A)
    def __sparse_inverse_operator(self,Mat):
        solvertype = self._solvertype
        if solvertype == "cg":
            return lambda b: splinalg.cg(Mat, b, tol=self._tol)[0]
        elif solvertype == "cg+amg":
            amg1 = time.time()
            amg = smoothed_aggregation_solver(Mat)
            amg2 = time.time()
            print "AMG setup ", (amg2-amg1), "sec"
            return lambda b: amg.solve(b, tol=self._tol, accel='cg')
        elif solvertype == "cg+ilu":
            ilu1 = time.time()
            ILUX = splinalg.spilu(Mat, fill_factor=10)
            ilu2 = time.time()
            print "ILU setup ", (ilu2-ilu1), "sec"
            return lambda b: splinalg.cg(Mat, b, tol=self._tol, M=ILUX)[0]
        elif solvertype == "superlu":
            splinalg.use_solver(useUmfpack=False)
            return lambda b: splinalg.spsolve(Mat,b)
        elif solvertype == "umfpack":
            splinalg.use_solver(useUmfpack=True)
            return lambda b: splinalg.spsolve(Mat,b)
        elif solvertype == "factorize+superlu":
            splinalg.use_solver(useUmfpack=False)
            t1 = time.time()
            linsolve = splinalg.factorized(Mat.tocsc())
            t2 = time.time()
            return linsolve
        elif solvertype == "factorize+umfpack":
            splinalg.use_solver(useUmfpack=True)
            linsolve = splinalg.factorized(Mat)
            return linsolve
        else:
            raise "unknown solver type"
    def __call__(self,b):
        b = np.asarray(b)
        r = np.asarray(np.zeros(len(b)))
        b = b[self._not_dirichlet]
        if self._pure_neumann:
            b = np.append(b,0) # sparse.vstack((b,0)).todense()
        x = self._apply_inverse(b)
        if self._pure_neumann :
            r[self._not_dirichlet] = x[:-1].flatten()
        else:
            r[self._not_dirichlet] = x.flatten()
        return r.reshape((len(r),1))

def sort_eig(v):
    e = np.array([v,range(len(v))]).transpose()
    l = e[v<0.0]
    h = e[v>=0.0]
    l = np.sort(l,axis=0)
    h = np.sort(h,axis=0)
    h[:,0] *= -1
    N = min(l.shape[0],h.shape[0])
    idx = np.zeros(2*N)
    idx[0::2] = l[:N,1]
    idx[1::2] = h[:N,1]
    return idx.astype(int)
