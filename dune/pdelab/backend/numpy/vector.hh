#ifndef NUMPY_VECTOR_HH
#define NUMPY_VECTOR_HH

#include <boost/python/object.hpp>
#include <boost/python/handle.hpp>
#if BOOST_VERSION < 106500
#include <boost/python/numeric.hpp>
#else
#include <boost/python/numpy.hpp>
#endif


#define NPY_NO_DEPRECATED_API NPY_1_10_API_VERSION
#include <numpy/arrayobject.h>

#include "type.hh"

namespace NumPy {

    namespace py = boost::python;
    using object = boost::python::object;

    template<typename E>
    class Vector
    {
    public:
        using Base = py::object;
        using value_type = E;
        using size_type = std::size_t;
        using iterator = E*;
        using const_iterator = const E*;

        Vector() :
            _array(allocate(0)),
            _raw_data(static_cast<E*>(PyArray_DATA((PyArrayObject*)_array.ptr()))),
            _size(0)
        {}

        // always make a deep copy!
        explicit Vector(const Vector & v) :
            _array(clone(v)),
            _raw_data(static_cast<E*>(PyArray_DATA((PyArrayObject*)_array.ptr()))),
            _size(v._size)
        {}

        Vector(size_type size) :
            _array(allocate(size)),
            _raw_data(static_cast<E*>(PyArray_DATA((PyArrayObject*)_array.ptr()))),
            _size(size)
        {}

        Vector(size_type size, const E & value) :
            _array(allocate(size)),
            _raw_data(static_cast<E*>(PyArray_DATA((PyArrayObject*)_array.ptr()))),
            _size(size)
        {
            std::fill(begin(), end(), value);
        }

        Vector & operator = (const Vector & v)
        {
            std::cout << "ASSIGN ";
            // either copy the data or clone
            if (_size == v._size)
            {
                int result = PyArray_CopyInto(
                    (PyArrayObject*)_array.ptr(),
                    (PyArrayObject*)v._array.ptr());
                if (result != 0)
                    DUNE_THROW(Dune::SystemError, "Failed to copy numpy array content");
            }
            else
            {
                _array = clone(v);
                _raw_data = static_cast<E*>(PyArray_DATA((PyArrayObject*)_array.ptr()));
                _size = v._size;
            }
            std::cout << v._raw_data << " -> " << _raw_data << std::endl;
            return *this;
        }

        size_type size () const
        {
            assert (PyArray_NDIM((PyArrayObject*)_array.ptr()) == 1);
            assert (PyArray_DIMS((PyArrayObject*)_array.ptr())[0] == _size);
            return PyArray_DIMS((PyArrayObject*)_array.ptr())[0];
        };

        void resize(size_type size)
        {
            _array = allocate(size);
            _raw_data = static_cast<E*>(PyArray_DATA((PyArrayObject*)_array.ptr()));
            _size = size;
        }

        void resize(size_type size, const E & value)
        {
            resize(size);
            std::fill(begin(), end(), value);
        }

        const E& front() const
        {
            return _raw_data[0];
        }

        const E& back() const
        {
            return _raw_data[_size-1];
        }

        E* begin()
        {
            return _raw_data;
        }
        E* end()
        {
            return _raw_data + _size;
        }

        const E* begin() const
        {
            return _raw_data;
        }
        const E* end() const
        {
            return _raw_data + _size;
        }

        E& operator[] (std::size_t pos)
        {
            return _raw_data[pos];
        }
        const E& operator[] (std::size_t pos) const
        {
            return _raw_data[pos];
        }

        boost::python::object object()
        {
            return _array;
        }
    private:
        boost::python::object _array;
        E* _raw_data;
        size_type _size;

        py::object allocate(size_type size)
        {
            npy_intp dims[1];
            dims[0] = size;
            PyObject* objp =
                PyArray_SimpleNew(1, dims, Type<E>::type);
            if (! objp)
                DUNE_THROW(Dune::SystemError, "Failed to allocate numpy array");
            py::handle<> handle(objp);
            py::object obj(handle);
            return obj;
        }

        py::object clone(const Vector & other)
        {
            py::object a = allocate(other.size());
            PyObject* po = other._array.ptr();
            assert(PyArray_CheckExact(po));
            PyObject* objp = PyArray_NewCopy((PyArrayObject*)po,NPY_CORDER);
            if (! objp)
                DUNE_THROW(Dune::SystemError, "Failed to clone numpy array");
            py::handle<> handle(objp);
            return py::object(handle);
        }
    };

    template<typename T>
    object vectorToPyObject(Vector<T> & data)
    {
        return data.object();
    }

} // end namespace NumPy

#endif // NUMPY_VECTOR_HH
