#ifndef NUMPY_TYPE_HH
#define NUMPY_TYPE_HH

#include <cstdint>
#include <Python.h>
#include <numpy/arrayobject.h>

namespace NumPy {

    struct Bool
    {
    private:
        bool val;
    public:
        Bool(bool v) : val(v) {}
        Bool() : val(0) {}
        operator bool() { return val; }
        Bool& operator= (bool v) { val = v; return *this; }
    };

    template<typename T>
    struct Type {};

    template<>
    struct Type<double> {
        static const int type = NPY_DOUBLE;
    };

    template<>
    struct Type<float> {
        static const int type = NPY_FLOAT;
    };

    template<>
    struct Type<int> {
        static const int type = NPY_INT;
    };

    template<>
    struct Type<uint8_t> {
        static const int type = NPY_UINT8;
    };

    template<>
    struct Type<Bool> {
        static const int type = NPY_BOOL;
    };

    template<>
    struct Type<unsigned int> {
        static const int type = NPY_UINT;
    };

    template<>
    struct Type<long> {
        static const int type = NPY_LONG;
    };

    template<>
    struct Type<unsigned long> {
        static const int type = NPY_ULONG;
    };

} // end namespace NumPy

#endif // NUMPY_TYPE_HH
