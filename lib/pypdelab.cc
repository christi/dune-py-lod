#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>
#include <string>
#include <algorithm>
#include <iterator>
#include <fstream>

#include <dune/common/math.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/common/gridinfo.hh>

#include <dune/pdelab/function/const.hh>
#include <dune/pdelab/common/function.hh>
#include <dune/pdelab/localoperator/l2.hh>

#include <dune/pdelab/elementmatrixadapter.hh>

#include <boost/python.hpp>
namespace py = boost::python;

//===============================================================
// General configuration
//===============================================================
static const int dim = DIM;
typedef Dune::YaspGrid<dim> BaseGrid;

#include "assembler.hh"
#include "pygrid.hh"

namespace Implementation {

    struct MassMatrixSetupBase
    {
        typedef Dune::PDELab::NoConstraints Constraints;
        typedef Dune::PDELab::NoConstraintsParameters ConstraintsParameters;
        typedef Dune::PDELab::L2 LocalOperator;
        LocalOperator lop() const
        {
            const int integration_order = 4;
            return LocalOperator(integration_order);
        }
        ConstraintsParameters constraints_parameters() const
        {
            return ConstraintsParameters();
        }
        template<typename GV>
        Dune::PDELab::ConstGridFunction<GV,double>
        dirichlet_extension(const GV & gv)
        {
            return Dune::PDELab::ConstGridFunction<GV,double> (gv,0.0);
        }
    };

}

template<typename GV, typename FEM, typename P>
struct MassMatrixSetup : public Implementation::MassMatrixSetupBase {
    typedef Implementation::MassMatrixSetupBase::Constraints Constraints;
    typedef Implementation::MassMatrixSetupBase::LocalOperator LocalOperator;
};

template<typename GV, SpaceType ElementWise>
py::object generate_massmatrix (const GridView<GV> & gv)
{
    return generate_matrix<GV,ElementWise, MassMatrixSetup, void*>(gv, 0);
}

class LocalGlobalMapping :
    public Dune::PDELab::LocalOperatorDefaultFlags
{
public:
    // pattern assembly flags
    enum { doPatternVolume = true };
    // residual assembly flags
    enum { doAlphaVolume = true };

    // define sparsity pattern of operator representation
    template<typename LFSU, typename LFSV, typename LocalPattern>
    void pattern_volume (const LFSU& lfsu, const LFSV& lfsv,
        LocalPattern& pattern) const
    {
        assert(lfsv.size() == lfsu.size());
        for (size_t i=0; i<lfsv.size(); ++i)
            pattern.addLink(lfsv,i,lfsu,i);
    }

    // jacobian of volume term
    template<typename EG, typename LFSU, typename X, typename LFSV, typename M>
    void jacobian_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv,
        M & mat) const
    {
        assert(lfsv.size() == lfsu.size());
        // Switches between local and global interface
        for (size_t i=0; i<lfsv.size(); i++)
            mat.accumulate(lfsv,i,lfsu,i,1);
    }
};

template<typename GV>
py::object generate_local_global_map (const GridView<GV> & wrapped_gv)
{
    GV gv = wrapped_gv.impl();

    // make finite element map
    typedef typename GV::Grid::ctype DF;
    typedef typename ::FEM<GV,DF,Conforming>::type FEM;
    FEM fem = ::FEM<GV,DF,Conforming>::instance(gv);
    typedef typename ::FEM<GV,DF,Discontinuous>::type DGFEM;
    DGFEM dgfem = ::FEM<GV,DF,Discontinuous>::instance(gv);

    // constants and types
    typedef typename FEM::Traits::FiniteElementType::Traits::
        LocalBasisType::Traits::RangeFieldType R;

    // make function space
    typedef Dune::PDELab::GridFunctionSpace<
        GV,
        FEM,
        Dune::PDELab::NoConstraints,
        Dune::PDELab::NumPyVectorBackend
        > GFS;
    GFS gfs(gv,fem);
    gfs.name("q1");
    gfs.update();

    // make dg function space
    typedef Dune::PDELab::GridFunctionSpace<
        GV,
        DGFEM,
        Dune::PDELab::NoConstraints,
        Dune::PDELab::NumPyVectorBackend
        > DGGFS;
    DGGFS dggfs(gv,dgfem);
    dggfs.name("element-wise");
    dggfs.update();

    // make local operator
    typedef LocalGlobalMapping LOP;
    LOP lop;

    // make grid operator
    typedef Dune::PDELab::GridOperator<DGGFS,GFS,LOP,
                                       Dune::PDELab::NumPyMatrixBackend,
                                       double,double,double
                                       > GridOperator;
    GridOperator gridoperator(dggfs,gfs,lop);

    // represent operator as a matrix
    typedef typename GridOperator::Traits::Jacobian M;
    M m(gridoperator, 1.0);

    // return matrix
    return Dune::PDELab::NumPy::getPyObject(m);
}

#include "pymappings.hh"
#include "pygrid.hh"

/* external register functions */
void register_models();

BOOST_PYTHON_MODULE(pypdelab)
{
    // initialize
    import_array();
    Dune::PDELab::NumPy::initialize();

    // register classes and functions
    import_grid();

    // mass matrix assembly
    py::def("generate_massmatrix", generate_massmatrix<Grid::Impl::LeafGridView, Conforming>,
        "store mass matrix in a scipy matrix.");
    py::def("generate_element_massmatrix", generate_massmatrix<Grid::Impl::LeafGridView, Discontinuous>,
        "store mass matrix in a numpy vector.");

    // projection operators
    py::def("generate_vertex_cell_info", generate_vertex_cell_info<Grid>,
        "generate a vector, which stores the mapping from levelGridView coarseLevel cells to leafGridView vertices.",
        (py::arg("grid"), py::arg("coarseLevel") = 0));
    py::def("generate_cell_cell_info", generate_cell_cell_info<Grid>,
        "generate a vector, which stores the mapping from levelGridView coarseLevel cells to leafGridView cells.",
        (py::arg("grid"), py::arg("coarseLevel") = 0));
    py::def("generate_local_global_map", generate_local_global_map<Grid::Impl::LeafGridView>,
        "generate a vector, which stores the mapping from leafGridView cell-vertices to leafGridView vertices (Sigma).");
    py::def("generate_basis_function_projection", generate_basis_function_projection<Grid>,
        "generate a matrix, which stores the representation of levelGridView coarseLevel basis functions in the leafGridView basis (Psi).",
        (py::arg("grid"), py::arg("coarseLevel") = 0));

    register_models();

    // map exceptions from dune to python
    py::register_exception_translator<Dune::Exception>([] (const Dune::Exception & e) { PyErr_SetString(PyExc_RuntimeError, std::string(e.what()).c_str()); });
}
