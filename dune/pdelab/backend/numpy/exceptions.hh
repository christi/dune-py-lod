#ifndef NUMPY_EXCEPTIONS_HH
#define NUMPY_EXCEPTIONS_HH

#include <Python.h>
#include <exception>

namespace NumPy {

struct PythonException : public std::exception {
    PythonException() {
        PyErr_Fetch(&ptype, &pvalue, &ptraceback);
    }
    virtual const char* what() const noexcept
    {
        // pvalue contains error message
        // ptraceback contains stack snapshot and many other information
        // (see python traceback structure)

        // Get error message
        if (PyString_Check(pvalue))
            return PyString_AsString(pvalue);
        else
            return "Unknown Python Error";
    }
    void restore() noexcept
    {
        PyErr_Restore(ptype, pvalue, ptraceback);
    }
    PyObject *ptype, *pvalue, *ptraceback;
};

} // end namespace NumPy

#define PyCheck if (PyErr_Occurred()) \
        throw PythonException();
#define PyAssert(A) if (! (A) ) \
        throw PythonException();

#endif // NUMPY_EXCEPTIONS_HH
