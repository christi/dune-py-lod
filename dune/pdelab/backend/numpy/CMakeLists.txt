set(commondir  ${CMAKE_INSTALL_INCLUDEDIR}/dune/pdelab/backend/numpy)
set(common_HEADERS
  deleter.hh
  exceptions.hh
  matrix.hh
  type.hh
  vector.hh)

install(FILES ${common_HEADERS} DESTINATION ${commondir})
