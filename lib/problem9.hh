struct Problem9
{
    static const bool has_exact_solution = true;
    static const bool pure_neumann = false;
    static const std::string name() { return "Problem9"; }

    template<typename GV, typename RF>
    class Parameters
    {
        typedef Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;

    public:
        typedef Dune::PDELab::ConvectionDiffusionParameterTraits<GV,RF> Traits;

        //! tensor diffusion coefficient
        typename Traits::PermTensorType
        A (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
        {
            typename Traits::DomainType x = e.geometry().global(xlocal);

            const RF Pi = Dune::MathematicalConstants<RF>::pi();
            const RF A00 = 2.0 * ( 1.0 / (8.0 * Pi * Pi) ) * ( 1.0 / ( 2.0 + cos( 2.0 * Pi * (x[0] / constants_epsilon) ) ) );
            const RF A11 = ( 1.0 / (8.0 * Pi * Pi) ) * ( 1.0 + ( 0.5 * cos( 2.0 * Pi * (x[0] / constants_epsilon) ) ) );

            return {{A00, 0.0},{0.0, A11}};
        }

        //! velocity field
        typename Traits::RangeType
        b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
        {
            typename Traits::RangeType v(0.0);
            return v;
        }

        //! sink term
        typename Traits::RangeFieldType
        c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
        {
            return 0.0;
        }

        //! source term
        typename Traits::RangeFieldType
        __attribute__((hot))
        f (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
        {
            typename Traits::DomainType x = e.geometry().global(xlocal);

            const RF Pi = Dune::MathematicalConstants<RF>::pi();
            const RF eps = constants_epsilon;
            const RF pi_square = pow(Pi, 2.0);
            const RF x0_eps = (x[0] / eps);
            const RF cos_2_pi_x0_eps = cos( 2.0 * Pi * x0_eps );
            const RF sin_2_pi_x0_eps = sin( 2.0 * Pi * x0_eps );
            const RF coefficient_0 = 2.0 * ( 1.0 / (8.0 * Pi * Pi) ) * ( 1.0 / ( 2.0 + cos_2_pi_x0_eps ) );
            const RF coefficient_1 = ( 1.0 / (8.0 * Pi * Pi) ) * ( 1.0 + ( 0.5 * cos_2_pi_x0_eps ) );
            const RF sin_2_pi_x0 = sin(2.0 * Pi * x[0]);
            const RF cos_2_pi_x0 = cos(2.0 * Pi * x[0]);
            const RF sin_2_pi_x1 = sin(2.0 * Pi * x[1]);

            const RF d_x0_coefficient_0
                = pow(2.0 + cos_2_pi_x0_eps, -2.0) * ( 1.0 / (2.0 * Pi) ) * (1.0 / eps) * sin_2_pi_x0_eps;

            const RF grad_u = (2.0* Pi* cos_2_pi_x0 * sin_2_pi_x1)
                +( (-1.0) * eps * Pi * ( sin_2_pi_x0 * sin_2_pi_x1 * sin_2_pi_x0_eps ) )
                +( Pi * ( cos_2_pi_x0 * sin_2_pi_x1 * cos_2_pi_x0_eps ) );


            const RF d_x0_x0_u =
                - (4.0 * pi_square * sin_2_pi_x0 * sin_2_pi_x1)
                - (2.0 * pi_square * ( eps + (1.0 / eps) ) * cos_2_pi_x0 * sin_2_pi_x1 * sin_2_pi_x0_eps)
                - (4.0 * pi_square * sin_2_pi_x0 * sin_2_pi_x1 * cos_2_pi_x0_eps);

            const RF d_x1_x1_u =
                - (4.0 * pi_square * sin_2_pi_x0 * sin_2_pi_x1)
                - (2.0 * pi_square * eps * cos_2_pi_x0 * sin_2_pi_x1 * sin_2_pi_x0_eps);

            return -(d_x0_coefficient_0 * grad_u)
                -(coefficient_0 * d_x0_x0_u)
                -(coefficient_1 * d_x1_x1_u);

        }

        //! boundary condition type function
        BCType
        bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
        {
            return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
        }

        //! Dirichlet boundary condition value
        typename Traits::RangeFieldType
        g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
        {
            return 0.0;
        }

        //! Neumann boundary condition
        typename Traits::RangeFieldType
        j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
        {
            assert(false && "wrong boundary condition");
            return 0.0;
        }

        //! outflow boundary condition
        typename Traits::RangeFieldType
        o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
        {
            assert(false && "wrong boundary condition");
            return 0.0;
        }
    };

    template<typename GV, typename RF>
    class ExactSolution
        : public Dune::PDELab::AnalyticGridFunctionBase<Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1>,
                                                        ExactSolution<GV,RF> >
    {
    public:
        typedef Dune::PDELab::AnalyticGridFunctionTraits<GV,RF,1> Traits;
        typedef Dune::PDELab::AnalyticGridFunctionBase<Traits,
                                                       ExactSolution<GV,RF> > BaseT;

        ExactSolution (const GV& gv) : BaseT(gv) {}
        inline void evaluateGlobal (const typename Traits::DomainType& x,
            typename Traits::RangeType& y) const
        {
            const RF Pi = Dune::MathematicalConstants<RF>::pi();
            // approximation obtained by homogenized solution + first corrector
            // coarse part
            y = - sin(2.0 * Pi * x[0]) * sin(2.0 * Pi * x[1]);

            // fine part // || u_fine_part ||_L2 = 0.00883883 (for eps = 0.05 )
            y += - 0.5 * constants_epsilon * ( cos(2.0 * Pi * x[0]) * sin(2.0 * Pi * x[1]) * sin( 2.0 * Pi * (x[0] / constants_epsilon) ) );

        } // evaluate
    };

}; // end Problem9
