struct spe10IO
{
    static double* readPermeability() {
        std::cout << ">>> Reading Permeability file" << std::endl;
        std::string filename = "spe10_permeability.dat";
        std::ifstream file(filename.c_str());
        double val;
        if (!file) { // file couldn't be opened
            DUNE_THROW(Dune::IOError, "file spe10_permeability.dat not found");
        }
        file >> val;
        int counter = 0;
        double* permeability = new double[3366000];
        while (!file.eof()) {
            // keep reading until end-of-file
            permeability[counter++] = val;
            file >> val; // sets EOF flag if no value found
        }
        file.close();
        return permeability;
    } /* readPermeability */

    static const double* permeability_;
};

template<bool pure_neumann>
struct spe10base : public spe10IO
{
    static const std::string name() {
        if (pure_neumann)
            return "spe10";
        return "spe10dirichlet";
    }

    template<typename GV, typename RF>
    class Parameters
    {
        typedef Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;

    public:
        typedef Dune::PDELab::ConvectionDiffusionParameterTraits<GV,RF> Traits;

        Parameters()
            : deltas_({6.096, 3.048, 0.6096})
            , permMatrix_(0.0)
        {
            if (permeability_ == nullptr)
                permeability_ = readPermeability();
        }

        //! tensor diffusion coefficient
        typename Traits::PermTensorType
        A (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
        {
            typename Traits::DomainType x = e.geometry().global(xlocal);

            for (int dim = 0; dim < Traits::DomainType::dimension; ++dim)
                permIntervalls_[dim] = std::floor(x[dim] / deltas_[dim]);

            int offset = 0;
            switch (int(Traits::dimDomain)) {
                case 1:
                    DUNE_THROW(Dune::NotImplemented, "spe10 is not implemented for 1D!");
                case 2:
                    offset = permIntervalls_[0] + permIntervalls_[1] * 60;
                    permMatrix_[0][0] = permeability_[offset];
                    permMatrix_[1][1] = permeability_[offset + 1122000];
                    break;
                case 3:
                    offset = permIntervalls_[0] + permIntervalls_[1] * 60 + permIntervalls_[2] * 220 * 60;
                    permMatrix_[0][0] = permeability_[offset];
                    permMatrix_[1][1] = permeability_[offset + 1122000];
                    permMatrix_[2][2] = permeability_[offset + 2244000];
                    break;
            }

            return permMatrix_;
        }

        //! velocity field
        typename Traits::RangeType
        b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
        {
            typename Traits::RangeType v(0.0);
            return v;
        }

        //! sink term
        typename Traits::RangeFieldType
        c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
        {
            return 0.0;
        }

        //! source term
        typename Traits::RangeFieldType
        f (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
        {
            return 0.0;
        }

        //! boundary condition type function
        BCType
        bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& xlocal) const
        {
            if (pure_neumann)
                return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Neumann;

            typename Traits::DomainType x = is.geometry().global(xlocal);

            if (x[1]-0.5*deltas_[1] < 1e-6)
            {
                // std::cout << "Dirichlet" << std::endl;
                return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
            }
            return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Neumann;
        }

        //! Dirichlet boundary condition value
        typename Traits::RangeFieldType
        g (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
        {
            // typename Traits::DomainType x = e.geometry().global(xlocal);
            // assert(false && "wrong boundary condition");
            return 0.0; // 1.0-x[1]/670.56;
        }

        //! Neumann boundary condition
        typename Traits::RangeFieldType
        j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& xlocal) const
        {
            typename Traits::DomainType x = is.geometry().global(xlocal);

            if (x[1] - 670.56 < 1e-6)
                return 1.0e-3;
            if (x[1]-0.5*deltas_[1] < 1e-6)
                return -1.0e-3;
            else
                return 0.0;
        }

        //! outflow boundary condition
        typename Traits::RangeFieldType
        o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
        {
            assert(false && "wrong boundary condition");
            return 0.0;
        }

    private:
        const std::vector<double> deltas_;
        mutable typename Traits::DomainType permIntervalls_;
        mutable typename Traits::PermTensorType permMatrix_;
    };

}; // end spe10

struct spe10 : public spe10base<true> {
    static const bool has_exact_solution = false;
    static const bool pure_neumann = true;
};
struct spe10dirichlet : public spe10base<false> {
    static const bool has_exact_solution = false;
    static const bool pure_neumann = false;
};
