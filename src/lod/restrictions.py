# -*- encoding: utf-8 -*-

import numpy as np
from scipy import sparse
import pypdelab as pde
import utils

class SubdomainInfo:
    """
    collect all information regarding the subdomain:
    - local constraints
    - indicator functions
    - restriciton operator
    - pure-neumann constraints (TODO)
    - local dirichlet constraints
    """
    def __init__(self, patchindicator, overlapindicator, dirichletindicator, cellvertexmap, coarsebasis = None) :
        """
        arguments:
        patchindicator   -- bool-vector marking cells which belong to the coarse cell
        overlapindicator -- bool-vector marking cells which belong to the subdomain       (see indicator_square)
        dirichletindicator -- bool-vector marking DOFs as Dirichlet-constraints    (see model.generate_dirichletinformation)
        cellvertexmap -- matrix mapping from cell-wise assembly to global assembly (see pypdelab.generate_local_global_map)
        """
        # get size info
        N_dofs  = len(dirichletindicator)
        N_cells = len(overlapindicator)
        celldofs = cellvertexmap.shape[1] / N_cells
        # Cell-Vertex Mapping operator
        self._E = cellvertexmap
        # Dirichlet-Indicator (symmetric)
        self._not_dirichlet = (dirichletindicator == False)
        # Subdomain-Cell-Indicator
        self._patchindicator = (patchindicator == True) # make this a bool vector
        self._overlapindicator = (overlapindicator == True) # make this a bool vector
        self._I = utils.diagonal_matrix(self._overlapindicator.repeat(celldofs))
        # Restriction-Matrix, eliminating empty/constraint rows
        self._restrict = (self._E*self._overlapindicator.repeat(celldofs)) != False
        # self._restrict_idx = np.squeeze(np.where(self._restrict))
        R = utils.diagonal_matrix(1. * self._restrict).tocsr()
        indptr = np.unique(R.indptr)
        self._R = sparse.csr_matrix((R.data,R.indices,indptr), shape=(len(indptr)-1,N_dofs))
        # compute local dirichlet indicator
        self._local_unconstraint_dofs = np.squeeze(self._R * (dirichletindicator + (self._E*(self._overlapindicator == False).repeat(celldofs))) == False)
        # compute local sigma contributions
        self._sigma = cellvertexmap * utils.diagonal_matrix(1. * self._patchindicator.repeat(celldofs))
        # if we know the coarse basis we also compute restrictions for the coarsebasis
        if not coarsebasis is None:
            RC = self._sigma * np.ones(self._sigma.shape[1])
            self._TH = self.__generate_coarse_restriction__(RC,coarsebasis)
            self._RH = self.__generate_coarse_restriction__(self._restrict,coarsebasis)
    @staticmethod
    def __generate_coarse_restriction__(indicator,Psi) :
        P = Psi * indicator.squeeze()
        col = np.array(np.where(P != 0)).squeeze() # compute indices of non-zero rows
        N = Psi.shape[0] # number of global coarse basis functions
        M = len(col) # number of local coarse basis functions
        row = np.array(range(M), np.int)
        val = np.ones(M)
        RH = sparse.coo_matrix((val, (row, col)), (M,N))
        return RH
    def TH(self):
        return self._TH
    def RH(self):
        return self._RH
    def R(self):
        """
        returns:
        restriction matrix, taking Dirichlet and sub-domain constraints into account
        """
        return self._R
    def Sigma(self):
        """
        returns:
        Sigma contributions associated with this coarse cell
        """
        return self._sigma
    def local_unconstraint_dofs(self):
        return self._local_unconstraint_dofs
    def restrict_matrix(self,A):
        assert A.shape == (len(self._restrict),len(self._restrict))
        return A[self._restrict,:][:,self._restrict] # double slicing
    def restrict_vector(self,b):
        return b[self._restrict]

def indicator_square(positions, H, origin, overlapfactor):
    """
    arguments:
    positions -- ndarray of shape (N,d) for N positions in a d dimensional space
    h         -- either a scalar value meshwidth (for the whole domain) or cell wise an ndarray of shape (N)
    origin    -- vector of size d with origin position
    overlapfactor -- scalar value for the overlap. Overlap is defined as multiples of h
    """
    epsilon = np.finfo(float).resolution
    if len(positions.shape) == 1:
        dim = 1
    else:
        assert(len(positions.shape) == 2)
        dim = positions.shape[1]
    if np.isscalar(H):
        hsize = 1
    else:
        assert(len(H.shape) == 1)
        hsize = H.shape[0]
    assert(hsize == 1 or hsize == dim or hsize == positions.shape[0])
    d = np.abs(positions-origin) < H*(.5+overlapfactor+epsilon)
    # reduce to 1d vector
    while len(d.shape) > 1:
        d = np.min(d,1)
    return d

@utils.timefunc
def generate_subdomain_info(grid, level, dirichletindicator, Sigma, H, overlap, Psi = None, indicatorfunction = indicator_square):
    gv = grid.leafGridView()
    coarse_gv = grid.levelGridView(level)
    dim = coarse_gv.dimension()
    coarse_positions = coarse_gv.cellpositions()
    coarse_positions = coarse_positions.reshape((coarse_positions.shape[0]/dim,dim))
    fine_positions = gv.cellpositions()
    fine_positions = fine_positions.reshape((fine_positions.shape[0]/dim,dim))
    N_patches = coarse_positions.shape[0]
    print "Generate cell/cell info"
    coarse_cell_map = pde.generate_cell_cell_info(grid,level)
    # print "Generate subdomain range for %i patches" % N_patches
    for i in range(N_patches):
        coarsecellindicator = (coarse_cell_map == i)
        subdomainindicator = indicatorfunction(fine_positions, H, coarse_positions[i], overlap)
        # print "Generate subdomain info %i" % i
        yield SubdomainInfo(coarsecellindicator, subdomainindicator, dirichletindicator, Sigma, Psi)
        coarsecellindicator = None
        subdomainindicator = None

def __test__():
    N = 4 # number of cells
    m = 2 # local DOFs
    M = N+1 # number of DOFs
    h = 1.0
    subdomain = 1
    print "N\t",N
    print "m\t",m
    print "M\t",M
    print "subdomain\t",subdomain

    print "\n# Construct Cell-Vertex to Vertex map:"
    print "\n# -> pypdelab.generate_local_global_map(g.leafGridView())"
    E = np.zeros(shape=(M,m*N))
    E = sparse.lil_matrix(E)
    for i in range(M):
        if 2*i-1>0:
            E[i,2*i-1] = 1
        if 2*i<N*m:
            E[i,2*i] = 1
    print E.todense()

    print "\n# Compute elementwise Stiffness Matrix AE:"
    print "\n# -> model.generate_element_stiffnessmatrix(g.leafGridView())"
    a = np.array([[1,-1],[-1,1]])
    data = np.tile(a.flatten(),N).reshape(N,m,m)
    indices = np.array(range(N))
    offsets = np.array(range(N+1))
    print data,indices,offsets
    AE = sparse.bsr_matrix((data,indices,offsets))
    print AE.todense()

    print "\n# Compute constraints indicator:"
    print "\n# -> model.generate_dirichletinformation(g.leafGridView())"
    dirichlet = np.zeros(M); dirichlet[0] = dirichlet[M-1] = 1
    not_dirichlet = - dirichlet + 1
    B = sparse.dia_matrix((not_dirichlet,[0]),shape=(M,M))
    print B.todense()

    print "\n# Compute Cell positions:"
    print "\n# -> g.levelGridView(l).cellpositions().reshape((N,dim))"
    print "\n# -> g.leafGridView().cellpositions().reshape((N,dim))"
    cell_pos = (np.array(range(0,N))+0.5) * h
    print cell_pos

    print "\n# Compute Subdomain Indicator function:"
    indicator = indicator_square(cell_pos,h,cell_pos[1],overlapfactor=1)
    print indicator

    print "\n# Compute Subdomain Info:"
    sInfo = SubdomainInfo(indicator=indicator, dirichletindicator=dirichlet, cellvertexmap=E)
    print " -> Subdomain Restriction"
    print "   ",sInfo._restrict
    print " -> Local Dirichlet Constraints"
    print "   ",sInfo._local_dirichlet

    print "\n# Compute localization of AE to patch:"
    A = sInfo.restrict(AE)
    print A.todense()
