namespace Implementation
{

    template<typename E>
    E& coarseGridEntity(E& e, int l)
    {
        while (e.level() != l)
        {
            e = e.father();
        }
        return e;
    }

    template<typename E, typename V>
    E& coarseGridEntity(E& e, int l, V & x)
    {
        while (e.level() != l)
        {
            x = e.geometryInFather().global(x);
            e = e.father();
        }
        return e;
    }

    /**
       assign subdomain index according to coarse grid index
    */
    template<typename FineGV, typename CoarseGV, typename RF>
    class SubDomainIndex
        : public Dune::PDELab::GridFunctionBase<
        Dune::PDELab::GridFunctionTraits<FineGV, RF, 1,
                                         Dune::FieldVector<RF,1> >, SubDomainIndex<FineGV, CoarseGV, RF> >
    {
    public:
        typedef Dune::PDELab::GridFunctionTraits<FineGV, RF, 1,
                                                 Dune::FieldVector<RF,1> > Traits;

        SubDomainIndex(const FineGV & fgv, const CoarseGV & cgv, int l) : fineGV(fgv), coarseGV(cgv), level(l) {}

        inline void evaluate (const typename Traits::ElementType& e,
            const typename Traits::DomainType& x,
            typename Traits::RangeType& y) const
        {
            typename CoarseGV::template Codim<0>::Entity entity(e);
            entity = coarseGridEntity(entity, level);
            y = coarseGV.indexSet().index(entity);
        }

        inline const typename Traits::GridViewType& getGridView () const
        {
            return fineGV;
        }

    private:
        const FineGV fineGV;
        const CoarseGV coarseGV;
        int level;
    };

    template<typename FineGV, typename CoarseGV, typename RF>
    class CoarseBasisIndex
        : public Dune::PDELab::GridFunctionBase<
        Dune::PDELab::GridFunctionTraits<FineGV, RF, (1<<CoarseGV::dimension),
                                                         Dune::FieldVector<RF,(1<<CoarseGV::dimension)> >, SubDomainIndex<FineGV, CoarseGV, RF> >
    {
    public:
        typedef Dune::PDELab::GridFunctionTraits<FineGV, RF, (1<<CoarseGV::dimension),
                                                                 Dune::FieldVector<RF,(1<<CoarseGV::dimension)> > Traits;

        CoarseBasisIndex(const FineGV & fgv, const CoarseGV & cgv, int l) : fineGV(fgv), coarseGV(cgv), level(l) {}

        inline void evaluate (const typename Traits::ElementType& e,
            const typename Traits::DomainType& x,
            typename Traits::RangeType& y) const
        {
            typename CoarseGV::template Codim<0>::Entity entity(e);
            entity = coarseGridEntity(entity, level);
            assert(entity.level() == level);
            // get index of vertices
            for (unsigned int i=0; i<entity.subEntities(CoarseGV::dimension); i++)
            {
                y[i] = coarseGV.indexSet().subIndex(entity, i, CoarseGV::dimension);
            }
        }

        inline const typename Traits::GridViewType& getGridView () const
        {
            return fineGV;
        }

    private:
        const FineGV fineGV;
        mutable CoarseGV coarseGV;
        int level;
    };

    /**
       represent a coarse basis function as a local function on a finer grid view
     */
    template<typename FineGV, typename CoarseGV, typename RF, typename FEM>
    class CoarseBasisFunction
        : public Dune::PDELab::GridFunctionBase<
        Dune::PDELab::GridFunctionTraits<FineGV, RF, (1<<CoarseGV::dimension),
                                                         Dune::FieldVector<RF,(1<<CoarseGV::dimension)> >, CoarseBasisFunction<FineGV, CoarseGV, RF, FEM> >
    {
    public:
        typedef Dune::PDELab::GridFunctionTraits<FineGV, RF, (1<<CoarseGV::dimension),
                                                                 Dune::FieldVector<RF,(1<<CoarseGV::dimension)> > Traits;

        CoarseBasisFunction(const FineGV & fgv, const CoarseGV & cgv, int l, const FEM & f) :
            fineGV(fgv), coarseGV(cgv), level(l), fem(f), out(1<<CoarseGV::dimension) {}

        inline void evaluate (const typename Traits::ElementType& e,
            const typename Traits::DomainType& x,
            typename Traits::RangeType& y) const
        {
            typename CoarseGV::template Codim<0>::Entity entity(e);
            typename Traits::DomainType X = x;
            entity = coarseGridEntity(entity, level, X);
            // Workaround an apparent YaspGrid bug
            {
                // vfry geometry mapping
                if ((e.geometry().global(x)-entity.geometry().global(X)).two_norm() > 1e-8)
                {
                    std::cerr << "Warning: mapping broken! " << e.geometry().global(x)
                              << " vs. "  << entity.geometry().global(X)
                              << "\tcell " << e.geometry().center()
                              << "\tat "  << x
                              << "\tmaps to " << X << std::endl;
                }
                X = entity.geometry().local(e.geometry().global(x));
            }
            // create basis
            const typename FEM::Traits::FiniteElementType& fe = fem.find(entity);
            // evaluate basis functions
            fe.localBasis().evaluateFunction(X,out);
            // copy data back
            assert(entity.subEntities(CoarseGV::dimension) == out.size());
            for (unsigned int i=0; i<out.size(); i++)
            {
                y[i] = out[i];
            }
        }

        inline const typename Traits::GridViewType& getGridView () const
        {
            return fineGV;
        }

    private:
        const FineGV fineGV;
        const CoarseGV coarseGV;
        int level;
        const FEM & fem;
        mutable std::vector<typename FEM::Traits::FiniteElement::
                            Traits::LocalBasisType::Traits::RangeType> out;
    };

} // end namespace Implementation

/**
   \brief generate a vector, which stores the mapping from levelGridView coarseLevel cells to leafGridView vertices.
*/
template<typename WrappedGrid>
py::object generate_vertex_cell_info (const WrappedGrid & wrapped_grid, int level)
{
    using Grid = typename WrappedGrid::Impl;
    const Grid & grid = wrapped_grid.impl();

    typedef typename Grid::LeafGridView GV;
    GV gv = grid.leafGridView();

    typedef typename Grid::LevelGridView CoarseGV;
    CoarseGV coarseGV = grid.levelGridView(level);

    // make finite element map
    typedef int RF;
    typedef typename GV::Grid::ctype DF;
    typedef Dune::PDELab::QkLocalFiniteElementMap<GV,DF,RF,1> FEM;
    FEM fem(gv);

    // constants and types
    typedef Dune::PDELab::NoConstraints CON;

    // make function space
    typedef Dune::PDELab::GridFunctionSpace<
        GV,
        FEM,
        CON,
        Dune::PDELab::NumPyVectorBackend
        > GFS;
    GFS gfs(gv,fem);
    gfs.name("solution");
    gfs.update();

    // make subdomain index operator
    typedef Implementation::SubDomainIndex<GV, CoarseGV, RF> IDX;
    IDX idx(gv, coarseGV, level);

    // evaluate subdomain index
#if DUNE_VERSION_NEWER(DUNE_PDELAB,2,4)
    using RV = Dune::PDELab::Backend::Vector<GFS,RF>;
#else
    typedef typename Dune::PDELab::BackendVectorSelector<GFS,RF>::Type RV;
#endif
    RV r(gfs);
    Dune::PDELab::interpolate(idx,gfs,r);
    // Dune::PDELab::set_constrained_dofs(cg,-1.0,r);

    // return numpy vector
    return Dune::PDELab::NumPy::getPyObject(r);
}

/**
   \brief generate a vector, which stores the mapping from levelGridView coarseLevel cells to leafGridView cells.
*/
template<typename WrappedGrid>
py::object generate_cell_cell_info (const WrappedGrid & wrapped_grid, int level)
{
    using Grid = typename WrappedGrid::Impl;
    const Grid &  grid = wrapped_grid.impl();

    typedef typename Grid::LeafGridView GV;
    GV gv = grid.leafGridView();

    typedef typename Grid::LevelGridView CoarseGV;
    CoarseGV coarseGV = grid.levelGridView(level);

    // make finite element map
    typedef int RF;
    typedef typename GV::Grid::ctype DF;
    typedef Dune::PDELab::P0LocalFiniteElementMap<DF,RF,dim> FEM;
    FEM fem(grid.leafGridView().template begin<0>()->type());

    // constants and types
    typedef Dune::PDELab::NoConstraints CON;

    // make function space
    typedef Dune::PDELab::GridFunctionSpace<
        GV,
        FEM,
        CON,
        Dune::PDELab::NumPyVectorBackend
        > GFS;
    GFS gfs(gv,fem);
    gfs.name("solution");
    gfs.update();

    // make subdomain index operator
    typedef Implementation::SubDomainIndex<GV, CoarseGV, RF> IDX;
    IDX idx(gv, coarseGV, level);

    // evaluate subdomain index
#if DUNE_VERSION_NEWER(DUNE_PDELAB,2,4)
    using RV = Dune::PDELab::Backend::Vector<GFS,RF>;
#else
    typedef typename Dune::PDELab::BackendVectorSelector<GFS,RF>::Type RV;
#endif
    RV r(gfs);
    Dune::PDELab::interpolate(idx,gfs,r);

    // return numpy vector
    return Dune::PDELab::NumPy::getPyObject(r);
}

/**
   \todo how to handle constraint coarse DOFs?
 */
template<typename WrappedGrid>
py::object generate_basis_function_projection (const WrappedGrid & wrapped_grid, int level)
{
    using Grid = typename WrappedGrid::Impl;
    const Grid & grid = wrapped_grid.impl();

    typedef typename Grid::LeafGridView GV;
    GV gv = grid.leafGridView();

    typedef typename Grid::LevelGridView CoarseGV;
    CoarseGV coarseGV = grid.levelGridView(level);

    // make finite element map
    typedef int IDX;
    typedef double RF;
    typedef typename GV::Grid::ctype DF;
    typedef Dune::PDELab::QkLocalFiniteElementMap<GV,DF,RF,1> FEM;
    typedef Dune::PDELab::QkLocalFiniteElementMap<CoarseGV,DF,RF,1> CoarseFEM;
    typedef Dune::PDELab::QkLocalFiniteElementMap<GV,DF,IDX,1> IDX_FEM;
    FEM fem(gv);
    CoarseFEM coarseFem(coarseGV);
    IDX_FEM idx_fem(gv);

    // constants and types
    typedef Dune::PDELab::NoConstraints CON;

    // make function space
    typedef Dune::PDELab::GridFunctionSpace<
        GV,
        FEM,
        CON,
        Dune::PDELab::NumPyVectorBackend
        > scalarGFS;
    typedef Dune::PDELab::PowerGridFunctionSpace<scalarGFS,(1<<dim)
        , Dune::PDELab::NumPyVectorBackend
        , Dune::PDELab::EntityBlockedOrderingTag
        > GFS;
    scalarGFS scalarGfs(gv,fem);
    GFS gfs(scalarGfs);
    gfs.name("value");
    gfs.update();

    // make integer function space
    typedef Dune::PDELab::GridFunctionSpace<
        GV,
        IDX_FEM,
        CON,
        Dune::PDELab::NumPyVectorBackend
        > scalarIDXGFS;
    typedef Dune::PDELab::PowerGridFunctionSpace<scalarIDXGFS,(1<<dim)
        , Dune::PDELab::NumPyVectorBackend
        , Dune::PDELab::EntityBlockedOrderingTag
        > IDXGFS;
    scalarIDXGFS scalarIdxGfs(gv,idx_fem);
    IDXGFS idxgfs(scalarIdxGfs);
    idxgfs.name("index");
    idxgfs.update();

    // make coarse basis operator
    typedef Implementation::CoarseBasisFunction<GV, CoarseGV, RF, CoarseFEM> FNKT;
    FNKT fnkt(gv, coarseGV, level, coarseFem);

    // make coarse basis index
    typedef Implementation::CoarseBasisIndex<GV, CoarseGV, IDX> IDX_FNKT;
    IDX_FNKT idx_fnkt(gv, coarseGV, level);

    // evaluate coarse function value
#if DUNE_VERSION_NEWER(DUNE_PDELAB,2,4)
    using RV = Dune::PDELab::Backend::Vector<GFS,RF>;
#else
    typedef typename Dune::PDELab::BackendVectorSelector<GFS,RF>::Type RV;
#endif
    RV val(gfs);
    Dune::PDELab::interpolate(fnkt,gfs,val);

    // evaluate coarse function index
#if DUNE_VERSION_NEWER(DUNE_PDELAB,2,4)
    using IV = Dune::PDELab::Backend::Vector<IDXGFS,IDX>;
#else
    typedef typename Dune::PDELab::BackendVectorSelector<IDXGFS,IDX>::Type IV;
#endif
    IV col(idxgfs);
    Dune::PDELab::interpolate(idx_fnkt,idxgfs,col);

    IV row(idxgfs);
    for (int i=0; i<row.base().size(); i++)
        row.base()[i] = i/(1<<dim);

    // return tuple of matrix and vector
    py::object py_val = Dune::PDELab::NumPy::getPyObject(val);
    py::object py_col = Dune::PDELab::NumPy::getPyObject(col);
    py::object py_row = Dune::PDELab::NumPy::getPyObject(row);
    return py::make_tuple(py_val, py_col, py_row);
}
