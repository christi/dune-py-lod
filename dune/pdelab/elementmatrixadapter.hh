#ifndef DUNE_PDELAB_ELEMENTMATRIXADAPTER_HH
#define DUNE_PDELAB_ELEMENTMATRIXADAPTER_HH

#include <cassert>

#include <dune/pdelab/gridoperator/common/localmatrix.hh>
#include <dune/pdelab/localoperator/flags.hh>

namespace Dune {
    namespace PDELab {

        template<class JacOp>
        class ElementMatrixAdapter : public LocalOperatorDefaultFlags
        {
        public:
            // residual assembly flags
            enum { doAlphaVolume = true };

            ElementMatrixAdapter (const JacOp & op)
                : _op(op)
            {}

            // volume integral depending on test and ansatz functions
            template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
            void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
            {
                typedef typename R::BaseContainer::value_type RF;
                typedef LocalMatrix<RF> Mat;
                typedef typename Mat::WeightedAccumulationView MatView;

                // size of the actual functionspace
                const int m=lfsv.size();
                // size of the flattened matrix
                const int n=lfsu.size();

                // make sure the local vector size equals the local matrix size
                assert( m == n*n );

                // Notice that in general lfsv.size() != mat.nrows()
                Mat mat(n,n,0.0);
                MatView view = mat.weightedAccumulationView(r.weight());

                // call JacOp::jacobian_volume
                _op.jacobian_volume(eg,lfsu,x,lfsv,view);

                // copy back to local vector
                for (unsigned int i=0; i<n; i++)
                    for (unsigned int j=0; j<n; j++)
                        // we assume we have a symmetric Galerkin Matrix
                        r.rawAccumulate(lfsv,i*n+j, mat(lfsu,i,lfsu,j));
            }
        private:
            const JacOp & _op;
        };

    } // end namespace PDELab
} // end namespace Dune

#endif // DUNE_PDELAB_ELEMENTMATRIXADAPTER_HH
