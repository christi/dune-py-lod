# setup copy commands for python files
set(PYFILES
  eigenvalues.py
  eigenvalues-ratio.py
  ellipticsource.py
  pypoisson.py
  )
add_subdirectory("test")
dune_symlink_to_source_files(FILES ${PYFILES})
