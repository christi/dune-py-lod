#!/usr/bin/python2.7 -u

import importlib

def version(name):
    m = importlib.import_module(name)
    v = m.__version__
    print "%s (version %s)" % (name, str(v))

print "Requirements:"
    
version("matplotlib")
version("numpy")
version("pandas")
version("scipy")
