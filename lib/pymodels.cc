#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#define NO_IMPORT_ARRAY

#include <dune/grid/yaspgrid.hh>
#include <dune/pdelab/localoperator/convectiondiffusionfem.hh>

#include <boost/python.hpp>
namespace py = boost::python;

//===============================================================
// General configuration
//===============================================================
static const int dim = DIM;
typedef Dune::YaspGrid<dim> BaseGrid;
static const double constants_epsilon = .031250; // 5e-2;

#include "pygrid.hh"
#include "assembler.hh"

#include "problem9.hh"
#include "problem14.hh"
#include "problem15.hh"
#include "problem16.hh"
#include "problem17.hh"
#include "ev-kp.hh"
#include "spe10.hh"

const bool Problem9::pure_neumann;
const bool Problem9::has_exact_solution;
const bool Problem14::pure_neumann;
const bool Problem14::has_exact_solution;
const bool Problem15::pure_neumann;
const bool Problem15::has_exact_solution;
const bool Problem16::pure_neumann;
const bool Problem16::has_exact_solution;
const bool Problem17::pure_neumann;
const bool Problem17::has_exact_solution;
const bool SimplifiedKronigPenney::pure_neumann;
const bool SimplifiedKronigPenney::has_exact_solution;
double     SimplifiedKronigPenney::k;
double     SimplifiedKronigPenney::ratio;
const bool spe10::pure_neumann;
const bool spe10::has_exact_solution;
const bool spe10dirichlet::pure_neumann;
const bool spe10dirichlet::has_exact_solution;
const double* spe10IO::permeability_ = nullptr;

template<typename GV, typename FEM, typename P>
struct PoissonSetup
{
private:
    typedef P Problem;
    typedef typename FEM::Traits::FiniteElementType::Traits::
        LocalBasisType::Traits::RangeFieldType R;
    typedef typename Problem::template Parameters<GV,R> LOPParam;
public:
    typedef Dune::PDELab::ConformingDirichletConstraints Constraints;
    typedef Dune::PDELab::ConvectionDiffusionBoundaryConditionAdapter<LOPParam> ConstraintsParameters;
    typedef Dune::PDELab::ConvectionDiffusionDirichletExtensionAdapter<LOPParam> DirichletExtension;
    typedef Dune::PDELab::ConvectionDiffusionFEM<LOPParam,FEM> LocalOperator;
    LocalOperator lop()
    {
        return LocalOperator(lopparam); // why a mutable reference?
    }
    ConstraintsParameters constraints_parameters() const
    {
        return ConstraintsParameters(lopparam);
    }
    DirichletExtension dirichlet_extension(const GV & gv)
    {
        return DirichletExtension(gv,lopparam); // why a mutable reference?
    }
private:
    LOPParam lopparam;
};

template<typename GV, typename Problem>
typename std::enable_if<Problem::has_exact_solution,
                        py::object>::type
generate_exact (const GridView<GV> & wrapped_gv, const Problem & problem)
{
    GV gv = wrapped_gv.impl();

    // make finite element map
    typedef typename GV::Grid::ctype DF;
    typedef Dune::PDELab::QkLocalFiniteElementMap<GV,DF,double,1> FEM;
    FEM fem(gv);

    // constants and types
    typedef typename FEM::Traits::FiniteElementType::Traits::
        LocalBasisType::Traits::RangeFieldType RF;
    typedef Dune::PDELab::NoConstraints CON;

    // make function space
    typedef Dune::PDELab::GridFunctionSpace<
        GV,
        FEM,
        CON,
        Dune::PDELab::NumPyVectorBackend
        > GFS;
    GFS gfs(gv,fem);
    gfs.name("solution");
    gfs.update();

    // make subdomain index operator
    typedef typename Problem::template ExactSolution<GV,RF> Exact;
    Exact exact(gv);

    // evaluate exact solution index
#if DUNE_VERSION_NEWER(DUNE_PDELAB,2,4)
    using RV = Dune::PDELab::Backend::Vector<GFS,RF>;
#else
    typedef typename Dune::PDELab::BackendVectorSelector<GFS,RF>::Type RV;
#endif
    RV r(gfs);
    Dune::PDELab::interpolate(exact,gfs,r);

    // return numpy vector
    return Dune::PDELab::NumPy::getPyObject(r);
}

// dummy if the model has no exact solution
template<typename GV, typename Problem>
typename std::enable_if<not Problem::has_exact_solution,
                        py::object>::type
generate_exact (const GridView<GV> & gv, const Problem & problem)
{
    assert(false);
}

template<typename GV, template<typename,typename,typename> class TSetup, typename Problem>
py::object generate_dirichletinformation (const GridView<GV> & wrapped_gv, const Problem & problem)
{
    GV gv = wrapped_gv.impl();

    std::cout << "GV " << &gv << std::endl;
    std::cout << "PROBLEM " << &problem << std::endl;
    // make finite element map
    typedef typename GV::Grid::ctype DF;
    typedef Dune::PDELab::QkLocalFiniteElementMap<GV,DF,double,1> FEM;
    FEM fem(gv);

    // make matrix setup descrition
    typedef TSetup<GV, FEM, Problem> Setup;
    Setup setup;

    // constants and types
    typedef typename FEM::Traits::FiniteElementType::Traits::
        LocalBasisType::Traits::RangeFieldType R;
    typedef Dune::PDELab::ConformingDirichletConstraints CON;

    // make function space
    typedef Dune::PDELab::GridFunctionSpace<
        GV,
        FEM,
        CON,
        Dune::PDELab::NumPyVectorBackend
        > GFS;
    GFS gfs(gv,fem);
    gfs.name("solution");
    gfs.update();

    // make constraints map and initialize it from a function
    typedef typename GFS::template ConstraintsContainer<R>::Type C;
    C cg;
    cg.clear();
    Dune::PDELab::constraints(setup.constraints_parameters(),gfs,cg);

    // make coefficent Vector and initialize it from a function
    // typedef typename Dune::PDELab::BackendVectorSelector<GFS,uint8_t>::Type DV;
    // typedef typename Dune::PDELab::BackendVectorSelector<GFS,NumPy::Bool>::Type DV;
#if DUNE_VERSION_NEWER(DUNE_PDELAB,2,4)
    using DV = Dune::PDELab::Backend::Vector<GFS,double>;
#else
    typedef typename Dune::PDELab::BackendVectorSelector<GFS,double>::Type DV;
#endif
    DV x0(gfs,false);
    Dune::PDELab::set_constrained_dofs(cg,true,x0);

    // return bool vector
    return Dune::PDELab::NumPy::getPyObject(x0);
}

template<typename Model>
py::class_<Model> register_model()
{
    // assembly operators
    py::def("generate_stiffnessmatrix", generate_matrix<Grid::Impl::LeafGridView, Conforming, PoissonSetup, Model>,
        "store stiffness matrix in a scipy matrix.");
    py::def("generate_element_stiffnessmatrix", generate_matrix<Grid::Impl::LeafGridView, Discontinuous, PoissonSetup, Model>,
        "store element operator in numpy vector.");
    py::def("generate_dirichletinformation", generate_dirichletinformation<Grid::Impl::LeafGridView, PoissonSetup, Model>,
        "store a bool flag in a numpy vector. The value is true if a vertex is a Dirichlet vertex.");
    py::def("generate_element_rhs", generate_rhs<Grid::Impl::LeafGridView, Discontinuous, Model>,
        "generate rhs for the poisson problem, store in a numpy vector.");
    if (Model::has_exact_solution)
        py::def("generate_exact", generate_exact<Grid::Impl::LeafGridView, Model>,
            "evaluate the exact solution.");
    return py::class_<Model>(Model::name().c_str(), py::init<>())
        .def_readonly("pure_neumann", &Model::pure_neumann)
        .def_readonly("has_exact_solution", &Model::has_exact_solution)
        .def("name", &Model::name)
        .staticmethod("name");
}

void register_models()
{
    // model classes and assembly operators
    register_model<Problem9>();
    register_model<Problem14>();
    register_model<Problem15>();
    register_model<Problem16>();
    register_model<Problem17>();
    register_model<SimplifiedKronigPenney>().def(py::init<double>()).def(py::init<double,double>());
    register_model<spe10>();
    register_model<spe10dirichlet>();
}
