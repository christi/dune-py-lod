struct Problem14
{
    static const bool has_exact_solution = false;
    static const bool pure_neumann = false;
    static const std::string name() { return "Problem14"; }

    template<typename GV, typename RF>
    class Parameters
    {
        typedef Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;

    public:
        typedef Dune::PDELab::ConvectionDiffusionParameterTraits<GV,RF> Traits;

        //! tensor diffusion coefficient
        typename Traits::PermTensorType
        A (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
        {
            typename Traits::DomainType x = e.geometry().global(xlocal);

            const RF Pi = Dune::MathematicalConstants<RF>::pi();
            const RF eps = constants_epsilon;
            const RF x0_eps = (x[0] / eps);
            const RF cos_2_pi_x0_eps = cos( 2.0 * Pi * x0_eps );

            RF a =
                1.1 + (0.5 * sin( std::floor( x[0] / eps) ) )
                + 0.5 * cos_2_pi_x0_eps;

            return {{a, 0.0},{0.0, a}};
        }

        //! velocity field
        typename Traits::RangeType
        b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
        {
            typename Traits::RangeType v(0.0);
            return v;
        }

        //! sink term
        typename Traits::RangeFieldType
        c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
        {
            return 0.0;
        }

        //! source term
        typename Traits::RangeFieldType
        __attribute__((hot))
        f (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
        {
            return 1.0;
        }

        //! boundary condition type function
        BCType
        bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
        {
            return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
        }

        //! Dirichlet boundary condition value
        typename Traits::RangeFieldType
        g (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
        {
            typename Traits::DomainType x = e.geometry().global(xlocal);

            const RF Pi = Dune::MathematicalConstants<RF>::pi();
            const RF eps = constants_epsilon;
            const RF x0_eps = (x[0] / eps);
            const RF x1_eps = (x[1] / eps);
            const RF sin_2_pi_x0_eps = sin( 2.0 * Pi * x0_eps );
            const RF cos_2_pi_x1_eps = cos( 2.0 * Pi * x1_eps );

            return sin_2_pi_x0_eps + cos_2_pi_x1_eps + 0.5 * exp( x[0] + x[1] );
        }

        //! Neumann boundary condition
        typename Traits::RangeFieldType
        j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
        {
            assert(false && "wrong boundary condition");
            return 0.0;
        }

        //! outflow boundary condition
        typename Traits::RangeFieldType
        o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
        {
            assert(false && "wrong boundary condition");
            return 0.0;
        }
    };

}; // end Problem14
