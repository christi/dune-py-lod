#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#define NO_IMPORT_ARRAY

#include <dune/grid/yaspgrid.hh>

#include <dune/pdelab/backend/numpy.hh>
#include <dune/pdelab/finiteelementmap/p0fem.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/interpolate.hh>

#include <boost/python.hpp>
namespace py = boost::python;

//===============================================================
// General configuration
//===============================================================
static const int dim = DIM;
typedef Dune::YaspGrid<dim> BaseGrid;
static const double constants_epsilon = 5e-2;

#include "pygrid.hh"

void grid_info(const Grid & g)
{
    Dune::gridinfo(g.impl());
}

namespace Implementation
{
    /**
       represent a coarse basis function as a local function on a finer grid view
     */
    template<typename GV>
    class CellPosition :
        public Dune::PDELab::AnalyticGridFunctionBase<
            Dune::PDELab::AnalyticGridFunctionTraits<GV,double,GV::dimensionworld>,
            CellPosition<GV> >
    {
        using Base =
            Dune::PDELab::AnalyticGridFunctionBase<
            Dune::PDELab::AnalyticGridFunctionTraits<GV,double,GV::dimensionworld>,
            CellPosition<GV> >;

    public:
        using Traits =
            Dune::PDELab::AnalyticGridFunctionTraits<GV,double,GV::dimensionworld>;

        CellPosition (const GV& g_) : Base(g_) {}

        inline void evaluate (const typename Traits::ElementType& e,
            const typename Traits::DomainType& x,
            typename Traits::RangeType& y) const
        {
            y = e.geometry().global(x);
        }
    };

} // end namespace Implementation

template<typename GV>
py::object GridView<GV>::cellpositions() const
{
    // make finite element map
    typedef double RF;
    typedef typename GV::Grid::ctype DF;
    typedef Dune::PDELab::P0LocalFiniteElementMap<DF,RF,GV::dimensionworld> FEM;
    FEM fem(gv_.template begin<0>()->type());

    // constants and types
    typedef Dune::PDELab::NoConstraints CON;

    // make function space
    typedef Dune::PDELab::GridFunctionSpace<
        GV,
        FEM,
        CON,
        Dune::PDELab::NumPyVectorBackend
        > scalarGFS;
    typedef Dune::PDELab::PowerGridFunctionSpace<
        scalarGFS,GV::dimensionworld,
        Dune::PDELab::NumPyVectorBackend,
        Dune::PDELab::EntityBlockedOrderingTag
        > GFS;
    scalarGFS scalarGfs(gv_,fem);
    GFS gfs(scalarGfs);
    gfs.update();

    // position function
    typedef Implementation::CellPosition<GV> POS;
    POS pos(gv_);

    // evaluate cell positions
#if DUNE_VERSION_NEWER(DUNE_PDELAB,2,4)
    using RV = Dune::PDELab::Backend::Vector<GFS,RF>;
#else
    typedef typename Dune::PDELab::BackendVectorSelector<GFS,RF>::Type RV;
#endif
    RV r(gfs);
    Dune::PDELab::interpolate(pos,gfs,r);

    // return numpy vector
    return Dune::PDELab::NumPy::getPyObject(r);
}

class VtkWriter : public Dune::SubsamplingVTKWriter<::Grid::Impl::LeafGridView>
{
    using Base = Dune::SubsamplingVTKWriter<::Grid::Impl::LeafGridView>;
    using Grid = ::Grid::Impl;
public:
    VtkWriter(const ::Grid::LeafGridView & gv, int refine = 2) :
        Base(gv.impl(),refine) {}
    void addCellData(py::object solution, const char* fnktname)
    {
        typedef Dune::P0VTKFunction<Grid::LeafGridView, std::vector<double>> Function;
        typedef Dune::VTKFunction<Grid::LeafGridView> VTKFunction;
        typedef std::shared_ptr<const VTKFunction> VTKFunctionPtr;
        VTKFunction* p = new Function(Base::gridView_, push_data(solution), std::string(fnktname), 1, 0);
        Base::addCellData(VTKFunctionPtr(p));
    }
    void addVertexData(py::object solution, const char* fnktname)
    {
        typedef Dune::P1VTKFunction<Grid::LeafGridView, std::vector<double>> Function;
        typedef Dune::VTKFunction<Grid::LeafGridView> VTKFunction;
        typedef std::shared_ptr<const VTKFunction> VTKFunctionPtr;
        VTKFunction* p = new Function(Base::gridView_, push_data(solution), std::string(fnktname), 1, 0);
        Base::addVertexData(VTKFunctionPtr(p));
    }
    void write(const char* filename)
    {
        Base::write(std::string(filename),Dune::VTK::appendedraw);
    }
private:
    const std::vector<double>& push_data(py::object solution)
    {
        // get raw data
        double * rawdata = (double*)PyArray_DATA((PyArrayObject*)solution.ptr());
        // copy into new vector
        data.push_back(std::make_shared< std::vector<double> >( PyArray_DIM((PyArrayObject*)solution.ptr(), 0) ) );
        memcpy(data.back()->data(), rawdata, sizeof(double)*data.back()->size());
        // return vector
        return *(data.back());
    }
    std::vector< std::shared_ptr<std::vector<double> > > data;
};

void import_grid()
{
    // grid related interfaces
    py::class_<Grid>("Grid", py::init<>())
        .def(py::init<py::list>())
        .def(py::init<py::list, py::list>())
        .def("globalRefine", &Grid::globalRefine, (py::arg("n") = 1))
        .def("dimension", &Grid::dimension)
        .def("levelGridView", &Grid::levelGridView, (py::arg("level")))
        .def("leafGridView", &Grid::leafGridView)
        .def("maxLevel", &Grid::maxLevel)
        .def("__str__", grid_info);
    py::class_<Grid::LeafGridView>("LeafGridView", py::no_init)
        .def("cellpositions", &Grid::LeafGridView::cellpositions)
        .def("dimension", &Grid::LeafGridView::dimension);
    py::class_<Grid::LevelGridView>("LevelGridView", py::no_init)
        .def("cellpositions", &Grid::LevelGridView::cellpositions)
        .def("dimension", &Grid::LevelGridView::dimension);
    py::class_<VtkWriter>("VtkWriter", py::init<Grid::LeafGridView>())
        .def(py::init<Grid::LeafGridView,int>())
        .def("addVertexData", &VtkWriter::addVertexData)
        .def("addCellData", &VtkWriter::addCellData)
        .def("write", &VtkWriter::write);
    py::def("grid_info", grid_info);
}
