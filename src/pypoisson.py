#!/usr/bin/env python

import sys
import os
import site
site.addsitedir(os.path.dirname(sys.argv[0])+"/..")

import math
import warnings

# initialize MPI (mayhaps we link DUNE against MPI?!)
from mpi4py import MPI

# import numpy/scipy
import numpy as np
import math
import scipy.sparse as sparse

# import DUNE
import pypdelab as pde
import lod
from lod import restrictions
from lod import timefunc
from lod import utils

@timefunc
def run_test(N,L,refine,level,overlap,write_vtk,model,verbose):
    overlapsize = int((1<<(refine-level))*overlap)    # overlap for patch
    H = (math.pow(0.5, level)*np.array(L)/np.array(N))
    # create grid and grid views
    print "create Grid"
    grid = pde.Grid(L,N)
    for i in range(refine):
        grid.globalRefine()
    gv = grid.leafGridView()
    coarse_gv = grid.levelGridView(level)
    dim = coarse_gv.dimension()
    print "generate local to global map"
    Sigma = timefunc(pde.generate_local_global_map)(gv)
    # generate sub domain info
    print "Mesh width:", H
    coarse_positions = coarse_gv.cellpositions()
    coarse_positions = coarse_positions.reshape((coarse_positions.shape[0]/dim,dim))
    fine_positions = gv.cellpositions()
    fine_positions = fine_positions.reshape((fine_positions.shape[0]/dim,dim))
    N_patches = coarse_positions.shape[0]
    print "generate fine mesh Dirichlet boundary information"
    dirichletindicator = pde.generate_dirichletinformation(gv,model)
    N_dofs = len(dirichletindicator)
    print "generate cell/cell info"
    coarse_cell_map = pde.generate_cell_cell_info(grid,level)
    ######################################################
    # get coarse-to-fine basis mapping in coordinate format
    print "generate basis functions projection"
    val, row, col = pde.generate_basis_function_projection(grid,level)
    Psi = sparse.coo_matrix((val, (row, col)))
    val, row, col = (None, None, None)
    ######################################################
    # generate subdomain info
    SubInfo = restrictions.generate_subdomain_info(grid,level,dirichletindicator,Sigma,H,overlap,Psi)
    # generate matrices
    print "generate element wise stiffness matrix and rhs"
    AE = timefunc(pde.generate_element_stiffnessmatrix)(gv,model)
    if model.name() == "Problem15":
        AE += utils.diagonal_matrix(np.ones(AE.shape[0])*1e-8)
    bE = timefunc(pde.generate_element_rhs)(gv,model)
    print "generate element wise massmatrix"
    ME = timefunc(pde.generate_element_massmatrix)(gv)
    print "generate global matrizes"
    M = Sigma * ME * Sigma.transpose()
    A = Sigma * AE * Sigma.transpose()
    ######################################################
    warnings.warn('fix dirichlet BC handling')
    print "generate fine mesh Dirichlet boundary corrections"
    b = Sigma * bE
    bh = np.logical_not(pde.generate_dirichletinformation(gv,model))
    Bh = utils.diagonal_matrix(bh)
    # lagrange interpolation operator from fine space into coarse space
    print "generate Lagrange restriction operator"
    Lag = (Psi == 1.0) * 1.0
    print "generate coarse mesh Dirichlet boundary corrections"
    BH = Lag * Bh * Lag.transpose()
    ######################################################
    # construct coarse space correction
    print "generate corrector functions"
    (Q_Psi,W) = lod.compute_correction(AE,bE,BH,M,Psi,Sigma,SubInfo,verbose,True)
    # restrict to coarse space
    P = Psi + Q_Psi
    # create sparse solver
    solve = timefunc(lod.spsolve)
    # setup vtk writer
    vtk = pde.VtkWriter(grid.leafGridView(),0)
    if write_vtk > 0:
        vtk.addCellData(1.0*coarse_cell_map,"coarse_cell")
    # solve coarse problem
    print "Solve coarse problem"
    x = Psi.transpose() * solve(Psi*A*Psi.transpose(),Psi*b,BH)
    if verbose > 1:
        print x
    if write_vtk > 0:
        vtk.addVertexData(1.0*bh,"bc_dirichlet")
        vtk.addVertexData(x,"u_coarse")
    # solve LOD problem
    print "Solve LOD problem"
    # W = W*0.0
    x = P.transpose()*solve(P*A*P.transpose(),P*(b),BH)
    if verbose > 1:
        print x
    if write_vtk > 0:
        vtk.addVertexData(x,"u_lod_nc")
        vtk.addVertexData(b,"b")
        vtk.addVertexData(Psi.transpose()*(Psi*b),"PsiTPsib")
        vtk.addVertexData(P.transpose()*(P*b),"PTPb")
        vtk.addVertexData(A*W,"AxW")
        vtk.addVertexData(W,"W")
    print "Solve corrected LOD problem"
    x = P.transpose()*solve(P*A*P.transpose(),P*(b-A*W),BH) + W
    # x = P.transpose()*solve(P*A*P.transpose(),P*(b-F),BH,"cg-ilu") + W
    if verbose > 1:
        print x
    if write_vtk > 0:
        vtk.addVertexData(x,"u_lod_c")
    # solve full problem
    print "Solve full problem"
    x_h = solve(A,b,Bh)
    if verbose > 1:
        print x_h
    warnings.warn("avoid large direct solve")
    if write_vtk > 0:
        vtk.addVertexData(x_h,"u_full")
    err = x-x_h
    print "error(u_lod) = %e" % math.sqrt(np.dot(err,(M*err)))
    print "error_rel(u_lod) = %e" % (math.sqrt(np.dot(err,(M*err)))/math.sqrt(np.dot(x_h,(M*x_h))))
    if model.has_exact_solution:
        x_exact = pde.generate_exact(grid.leafGridView(), model)
        if write_vtk > 0:
            vtk.addVertexData(x_exact,"u_exact")
        err = x-x_exact
        print "error_ana(u_lod) = %e" % math.sqrt(np.dot(err,(M*err)))
    if write_vtk > 0:
        vtk.addVertexData(err,"error")
        name = "pypoisson-%s-%i-%i-%i" % (model.name(),refine,level,overlapsize)
        print ">>> Write VTK output %s.vtu" % name
        vtk.write(name)
    if write_vtk > 1:
        Psi = Psi.tocsr()
        P = P.tocsr()
        rl = np.zeros((len(Rs),4,Psi.shape[1]))
        for i in range(len(Rs)) : # for each cell
            tmp = - (THs[i] *
                     BH * Psi * SigmaKs[i] * AE * SigmaKs[i].transpose())
            rl[i] += tmp
        for i in range(len(Rs)) : # for each cell
            vtk = pde.VtkWriter(grid.leafGridView(), 0)
            assert Rs[i].shape[1] == REs[i].shape[1]
            sz = Rs[i].shape[1]
            v = np.ones(sz)
            x = REs[i].transpose() * REs[i] * v
            vtk.addVertexData(REs[i].transpose() * REs[i] * v,"cell")
            vtk.addVertexData(Rs[i].transpose() * Rs[i] * v,"overlap_region")
            vtk.addVertexData(rl[i,0],"loadvector 0")
            vtk.addVertexData(rl[i,1],"loadvector 1")
            vtk.addVertexData(rl[i,2],"loadvector 2")
            vtk.addVertexData(rl[i,3],"loadvector 3")
            vtk.addVertexData(REs[i].transpose() * REs[i] * v
                              +
                              Rs[i].transpose() * Rs[i] * v
                              +
                              R2s[i].transpose() * R2s[i] * v,"support")
            vtk.addVertexData(Psi.todense().cumsum(0)[-1:].transpose(), "Psi partition")
            vtk.addVertexData((-1.0*bh+1.0),
                              "bc_dirichlet")
            vtk.addVertexData(Rs[i].transpose()*Rs[i]*(-1.0*bh+1.0),
                              "bc_d1")
            vtk.addVertexData(R2s[i].transpose()*R2s[i]*1.0*(-1.0*bh+1.0),
                              "bc_d2")
            print ">>> Write Cell VTK files"
            vtk.write("Cell-%i-%i-%i-%i" % (refine,level,overlapsize,i))
        for i in range(Q_Psi.shape[0]) :
            vtk = pde.VtkWriter(grid.leafGridView(),0)
            q = (Q_Psi[i].todense().getA().reshape(Q_Psi.shape[1]))
            vtk.addVertexData(q,
                              "Q(Psi)")
            vtk.addVertexData(Psi[i].todense().getA().reshape(Psi.shape[1]),
                              "Psi")
            vtk.addVertexData(P[i].todense().getA().reshape(P.shape[1]),
                              "Psi+Q")
            vtk.addVertexData(1.0*((P[i].todense().getA().reshape(P.shape[1]))!= 0.0) +
                              1.0*((Psi[i].todense().getA().reshape(Psi.shape[1]))!= 0.0), "support")
            vtk.addVertexData(1.0*((Q_Psi[i].todense().getA().reshape(Q_Psi.shape[1]))!= 0.0),"active_vertices")
            vtk.addCellData(1.0*coarse_cell_map,"coarse_cell_map")
            print ">>> Write corrector VTK files"
            vtk.write("pypoisson-QPSI-%s-%i-%i-%i-%i" % (model.name(),refine,level,overlapsize,i))

eps = 0.01

def performance_coarse_level(model, l = 7, write_vtk = 0) :
    L = [1,1]
    for i in range(1,l-1):
        print "--", l, "--", i, "----------------------"
        run_test(N=[1,1], L=L, refine=l, level=i, overlap=.0, model=model, write_vtk=write_vtk, verbose=0)
        print "--------------------------------"

def compare_overlap(model, R = 7, l = 2, write_vtk = 0) :
    for o in np.arange(.5,4.0+eps,.5):
        print "--", R, "--", l, "--", o,"-----------------"
        run_test(N=[1,1], L=[1,1], refine=R, level=l, overlap=o, model=model, write_vtk=write_vtk, verbose=0)
        print "--------------------------------"

def convergence_test(model, R = 7, lmax = None, c = 0.5, write_vtk = 0) :
    if lmax == None:
        lmax = R-1
    for l in np.arange(1,lmax+1):
        from math import log1p
        o = c * l # log1p(l)
        print "--", R, "--", l, "--", o, "-----------------"
        run_test(N=[1,1], L=[1,1], refine=R, level=l, overlap=o, model=model, write_vtk=write_vtk, verbose=0)
        print "--------------------------------"

def full_test_run_as_in_paper(write_vtk=0):
    p9 = pde.Problem9()
    R = 6
    L = 4
    O = [0,.5,1.5,2,3]
    for l in range(1,L+1):
        for o in np.arange(.5,O[l]+.5+eps,.5):
            print "--", R, "--", l, "--", o,"-----------------"
            run_test(N=[1,1], L=[1,1], refine=R, level=l, overlap=o, model=p9, write_vtk=write_vtk, verbose=0)
            print "--------------------------------"

def main():
    timefunc.enable = True
    model = pde.Problem14()
    print "running test for ", model.name()
    print model.has_exact_solution
    # compare_overlap(model,6,2,1)
    # run_test(N=[1,1], L=[1,1], refine=6, level=2, overlap=1, model=model, write_vtk=1, verbose=0)
    # run_test(N=[1,1], L=[1,1], refine=7, level=2, overlap=10, model=model, write_vtk=1, verbose=0)
    run_test(N=[1,1], L=[1,1], refine=5, level=0, overlap=10, model=model, write_vtk=1, verbose=0)
    run_test(N=[1,1], L=[1,1], refine=5, level=1, overlap=10, model=model, write_vtk=1, verbose=0)
    run_test(N=[1,1], L=[1,1], refine=5, level=2, overlap=10, model=model, write_vtk=1, verbose=0)
    # run_test(N=[1,1], L=[1,1], refine=4, level=1, overlap=10, model=model, write_vtk=1, verbose=0)
    # run_test(N=[1,1], L=[1,1], refine=7, level=4, overlap=1, model=model, write_vtk=0, verbose=0)
    # run_test(N=[1,1], L=[1,1], refine=8, level=3, overlap=1, model=model, write_vtk=1, verbose=0)
    # run_test(N=[1,1], L=[1,1], refine=8, level=4, overlap=1, model=model, write_vtk=1, verbose=0)
    # run_test(N=[1,1], L=[1,1], refine=6, level=2, overlap=10, model=model, write_vtk=1, verbose=0)
    # run_test(N=[1,1], L=[1,1], refine=6, level=2, overlap=0.5, model=model, write_vtk=1, verbose=0)
    # run_test(N=[1,1], L=[1,1], refine=6, level=2, overlap=1.0, model=model, write_vtk=1, verbose=0)
    #convergence_test(p9,8)
    return
    for l in range(1,7):
        run_test(N=[1,1], L=[1,1], refine=6, level=l, overlap=1.0, model=model, write_vtk=1, verbose=0)

if __name__ == "__main__":
    np.set_printoptions(threshold=np.inf)
    main()
