#ifndef DUNE_PDELAB_BACKEND_NUMPY_HH
#define DUNE_PDELAB_BACKEND_NUMPY_HH

#include <dune/pdelab/backend/simple.hh>
#include <vector>

#include <boost/python/exception_translator.hpp>

#include "numpy/vector.hh"
#include "numpy/matrix.hh"

namespace Dune {
    namespace PDELab {

        template<typename E>
        using numpy_vector = ::NumPy::Vector<E>;
        // using numpy_vector = std::vector<E>;
        typedef SimpleVectorBackend< numpy_vector > NumPyVectorBackend;
        typedef SimpleSparseMatrixBackend< numpy_vector, int > NumPyMatrixBackend;

        namespace NumPy {

            inline void initialize()
            {
                using namespace boost::python;
                register_exception_translator<Dune::Exception>(
                    [] (const Dune::Exception & e) { PyErr_SetString(PyExc_RuntimeError, std::string(e.what()).c_str()); });
#if BOOST_VERSION < 106500
		// Specify that py::numeric::array should refer to the Python type numpy.ndarray
		// (rather than the older Numeric.array).
		boost::python::numeric::array::set_module_and_type("numpy", "ndarray");
#endif
            }

            /**
               create a PyObject from a NumPyVector/SimpleVector

               \param vector the VectorContainer provided by the NumPyVectorBackend
               \note  the vector has to be mutable, as the conversion to PyObject requires mutable data
            */
            template<typename GFS, typename E>
            boost::python::object
            getPyObject(simple::VectorContainer<GFS,numpy_vector<E> > & vector)
            {
                return ::NumPy::vectorToPyObject(vector.base());
            }

            /**
               create a PyObject from a NumPyMatrix/SimpleSparseMatrix

               \param matrix the VectorContainer provided by the NumPyMatrixBackend
               \note  the matrix has to be mutable, as the conversion to PyObject requires mutable data
            */
            template<typename GFSU, typename GFSV, typename E>
            boost::python::object getPyObject(simple::SparseMatrixContainer<GFSV,GFSU,numpy_vector,E,int> & matrix)
            {
                auto & matrix_container = Backend::native(matrix);
                return ::NumPy::createCSRMatrix(
                    matrix_container._data,
                    matrix_container._colindex,
                    matrix_container._rowoffset);
            }

        } // end namespace NumPy

    } // end namespace PDELab
} // end namespace Dune

#endif // DUNE_PDELAB_BACKEND_NUMPY_HH
