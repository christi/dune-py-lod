# -*- encoding: utf-8 -*-

import warnings

# import numpy/scipy
import numpy as np
import scipy
import scipy.sparse as sparse

import time
import utils
import linalg
from utils import timefunc

@timefunc
def compute_correction(Adc, fdc, BH, Mh, Ph, Sigma, SubInfo, verbose=1, print_kernel=False) :
    '''generate the correction Q[i] associated coarse space basis functions

    Arguments:
    Adc -- Sparse matrix with element contributions to the stiffness matrix
    fdc -- Vector with element contributions to the right-hand-side
    BH  -- Coarse-Mesh constraints matrix (parameter has to change!)
    Mh  -- Fine-Mesh Mass-matrix
    Ph  -- Fine-Mesh representation of coarse-mesh basis functions
    Sigma -- 
    SubInfo -- list of per sub-domain structures
               SubInfo.R()  -- fine space restriction to patch
               SubInfo.RH   -- coarse space restriction to patch
               SubInfo.TH   -- coarse space restriction to cell
    verbose      -- Set verbosity level ([0-4], default=1)
    print_kernel -- Enable/disable output of Kernel-residual, used for debugging (default=False)
    '''
    # linear solver statistics
    total_Al_inverse_time = 0
    total_Al_inverse_calls = 0
    constructionTime = 0
    # some helper functions
    # # === restrict coarse information to unconstraint DOFs
    # Ph = BH * Ph
    # === get sizes
    Nh = Ph.shape[1]    # global fine grid problem size
    NH = Ph.shape[0]    # global coarse grid problem size
    NTH = 0 # len(SubInfo)  # coarse grid size
    # === compute operators ===
    Ah = (Sigma * Adc * Sigma.transpose()).tocsr()
    assert Ah.shape == (Nh,Nh) # rows x cols
    assert Mh.shape == (Nh,Nh) # rows x cols
    # === setup clement operator
    Ph = Ph.tocsr()
    MH = Ph * Mh * Ph.transpose()
    assert MH.shape == (NH,NH)
    # since the scaling diag(MH 1)^-1 does not change the kernel (which is all we need)
    # we can simply use...
    Ch = BH * Ph * Mh
    assert Ch.shape == (NH, Nh)
    # === storage for the computed corrections
    Q = sparse.lil_matrix((NH,Nh))
    W = np.matrix(np.zeros([Nh,1]))
    # === solve subdomain problems
    l = 0
    for l,sub in enumerate(SubInfo):
        if verbose > 0:
            print "Coarse cell", l,
        start = time.time()
        Rhl = sub.R()
        THl = sub.TH()
        RHl = sub.RH()
        Psi = (THl * BH * Ph).tocsr() # local cell basis functionse (not patch!)
        # == local problem configuration
        cd  = THl.shape[0]  # coarse cell basis functions
        Nlh = Rhl.shape[0]  # subdomain problem size
        NlH = RHl.shape[0]  # subdomain coarse basis functions
        assert Rhl.shape == (Nlh,Nh) # rows x cols
        assert THl.shape == (cd, NH) # rows x cols
        assert RHl.shape == (NlH,NH) # rows x cols
        # be verbose
        if verbose > 0:
            print "Subproblem patch %i\t(N_h=%i, N_H=%i, N_lh=%i, N_lH=%i)" % (l, Nh, NH, Nlh, NlH)
        if verbose > 2:
            print Rhl.todense()
        # === compute local matrices
        # Stiffness matrix
        RhlT = Rhl.transpose()
        Al = sub.restrict_matrix(Ah)
        assert Al.shape == (Nlh, Nlh)
        # Clement matrix (dense matrix) (eliminate empty rows)
        Cl = utils.eliminate_empty_rows(RHl * Ch * RhlT).todense()
        NClH = Cl.shape[0]
        if not (Cl.shape[1] == Nlh and Cl.shape[0] <= NlH and Cl.shape[0] > 0):
            print Cl.shape
            print (Nlh,Nlh)
            print "Cl.shape[1] == Nlh and Cl.shape[0] <= NlH and Cl.shape[0] > 0"
            import sys
            sys.exit(0)
        assert Cl.shape[1] == Nlh and Cl.shape[0] <= NlH and Cl.shape[0] > 0
        ### short cut for full-overlap
        if l == 0 or Nh != Nlh:
            # ...
            Y = np.matrix(np.zeros(Cl.shape)) # this matrix is more or less dense
            # setup sparse solver
            Al_inverse = linalg.InverseOperator(Al,sub.local_unconstraint_dofs())
            # compute inverse Schur Complement
            # precomputations for each coarse basis basis functions Psi_j
            for i in range(Cl.shape[0]):
                # compute RHS via the weighted clement operator
                t1 = time.time()
                Y[i] = Al_inverse(Cl[i].transpose()).transpose()
                t2 = time.time()
                total_Al_inverse_calls += 1
                total_Al_inverse_time += t2 - t1
            # compute LU decomposition of the Schur-Complement matrix
            Sl = Y * Cl.transpose() # eq to Sl = Cl * Y.transpose()
            if verbose > 2:
                print "Sl =\n", Sl
            assert Sl.shape == (NClH, NClH)
            startLU = time.time()
            Sl_factor = scipy.linalg.lu_factor(Sl)
            Sl_inverse = lambda x : scipy.linalg.lu_solve(Sl_factor, x, overwrite_b=True)
            endLU = time.time()
            if verbose > 0:
                print 'LU decomposition of Schur complement matrix ... took', endLU - startLU, 'sec'
        start2 = time.time()
        # compute correction for each coarse basis function
        for i in range(cd):
            if verbose > 0:
                print ".",
            idx = THl.nonzero()[1]
            assert(BH.diagonal()[idx[i]] == 1 or BH.diagonal()[idx[i]] == 0)
            if BH.diagonal()[idx[i]] != 1:
                continue
            # compute r based on element stiffness matrix entries
            # r = - (Rhl * Bh * sub.Sigma() * Adc * sub.Sigma().transpose() * Ph[idx[i]].transpose()).todense()
            r = - (Rhl * sub.Sigma() * Adc * sub.Sigma().transpose() * Ph[idx[i]].transpose()).todense()
            t1 = time.time()
            q = Al_inverse(r)
            t2 = time.time()
            # solve for v
            rhs_v = Cl * q
            assert rhs_v.shape[0] == NClH
            lmda = Sl_inverse(rhs_v)
            assert lmda.shape[0] == NClH
            # update
            # compute local contribution w to global correction of basis function
            t1 += time.time()
            # w = Al_inverse(q - ClT * v) # A^-1 * (q - ClT * v)
            w = q - Y.transpose() * lmda
            t2 += time.time()
            if verbose > 3:
                print "w ="
                print w.transpose()
            # corrector is given as the sum of all local correctors
            if print_kernel:
                print "Kern global? %e" % np.linalg.norm((Ch * (RhlT * w)), np.inf)
                print "Kern local ? %e" % np.linalg.norm((Cl * w), np.inf)
            Q[idx[i]] += (RhlT * w).transpose()
            total_Al_inverse_calls += 2
            total_Al_inverse_time += t2 - t1
        # compute correction for RHS
        r = Rhl * sub.Sigma() * fdc
        q = Al_inverse(r)
        v = Sl_inverse(Cl * q)
        w = q - Y.transpose() * v
        W += (RhlT * w)
        if verbose > 0:
            print "!"
        end = time.time()
        constructionTime += end-start2
        if verbose > 0:
            print '... took', end - start, 'sec'
        sub = None # reset memory
    endTime = time.time()
    l += 1 # count how many coarse cells we have
    print "time: " , constructionTime , "sec / ", l, " coarse cells inner loops"
    print "time total: " , total_Al_inverse_time , "sec / ", total_Al_inverse_calls
    print "time per solve: " , total_Al_inverse_time / total_Al_inverse_calls
    # solve coarse space problem
    if verbose > 1:
        print "Q ="
        print Q.todense()
    W = np.squeeze(np.asarray(W))
    return (Q,W)
