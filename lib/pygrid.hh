#ifndef DUNE_PY_GRID_HH
#define DUNE_PY_GRID_HH

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/common/gridinfo.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include <dune/grid/io/file/vtk/function.hh>

#include <Python.h>

template<typename GV>
class GridView {
private:
    GV gv_;
public:

    using Impl = GV;

    GridView(const GV & gv) :
        gv_(gv)
    {}

    py::object cellpositions() const;

    GV & impl()
    {
        return gv_;
    }

    const GV & impl() const
    {
        return gv_;
    }

    int dimension() const { return GV::dimension; }

};

class Grid
{
    using LeafGridViewBase  = BaseGrid::LeafGridView;
    using LevelGridViewBase = BaseGrid::LevelGridView;
public:
    using Impl = BaseGrid;
    using LeafGridView  = GridView<LeafGridViewBase>;
    using LevelGridView = GridView<LevelGridViewBase>;

    Grid ()
    {
        Dune::FieldVector<double,dim> L(1.0);
        Dune::array<int,dim> N;
        for (int d = 0; d < dim; d++) N[d] = 1;
        g = new BaseGrid(L,N);
        std::cout << "created Grid " << g << std::endl;
    }
    Grid (py::list _N)
    {
        Dune::FieldVector<double,dim> L(1.0);
        assert(py::len(_N) == dim);
        Dune::array<int,dim> N;
        for (int d = 0; d < dim; d++) N[d] = py::extract<int>(_N[d]);
        g = new BaseGrid(L,N);
        std::cout << "created Grid " << g << std::endl;
    }
    Grid (py::list _L, py::list _N)
    {
        assert(py::len(_N) == dim);
        Dune::FieldVector<double,dim> L;
        for (int d = 0; d < dim; d++) L[d] = py::extract<float>(_L[d]);
        assert(py::len(_N) == dim);
        Dune::array<int,dim> N;
        for (int d = 0; d < dim; d++) N[d] = py::extract<int>(_N[d]);
        g = new BaseGrid(L,N);
        std::cout << "created Grid " << g << std::endl;
    }
    ~Grid ()
    {
        std::cout << "destroy Grid " << g << std::endl;
        delete g;
    }

    int dimension() const { return BaseGrid::dimension; }

    void globalRefine(int i) const
    {
        g->globalRefine(i);
    }

    int maxLevel () const
    {
        return g->maxLevel();
    }

    LevelGridView levelGridView (int level) const
    {
        return g->levelGridView(level);
    }

    LeafGridView leafGridView () const
    {
        return g->leafGridView();
    }

    const BaseGrid & impl() const
    {
        return *g;
    }

    BaseGrid & impl()
    {
        return *g;
    }

private:
    BaseGrid * g;
};

void import_grid();

#endif // DUNE_PY_GRID_HH
