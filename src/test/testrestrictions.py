#!/usr/bin/python -u
# -*- encoding: utf-8 -*-

import sys
import os
import site
site.addsitedir(os.path.dirname(sys.argv[0])+"/..")

import math
import warnings
from IPython import embed
import cProfile

# initialize MPI (mayhaps we link DUNE against MPI?!)
from mpi4py import MPI

# import numpy/scipy
import numpy as np
import scipy
import scipy.sparse as sparse
from scipy.sparse import linalg as splinalg

# import DUNE
import pypdelab as pde
import lod
from lod import restrictions
from lod import timefunc

def set_dirichlet(N,refine,where):
    shape = np.array(N)*(1<<refine)+1
    N_dofs = shape[0] * shape[1]
    dirichletindicator = np.zeros(shape)
    if where[0][0] == 1: dirichletindicator[0,:] = 1
    if where[0][1] == 1: dirichletindicator[-1,:] = 1
    if where[1][0] == 1: dirichletindicator[:,0] = 1
    if where[1][1] == 1: dirichletindicator[:,-1] = 1
    return dirichletindicator.transpose().reshape(N_dofs)

@timefunc
def run_test(N,L,refine,level,overlap,write_vtk):
    lod.time_measurement = True
    overlapsize = int((1<<(refine-level))*overlap)    # overlap for patch
    print "Overlap size %i" % overlapsize
    overlapsizeX = overlapsize+1 # overlap for clement operator
    # create grid and grid views
    grid = pde.Grid(L,N)
    dim = grid.dimension()
    for i in range(refine):
        grid.globalRefine()
    gv = grid.leafGridView()
    H = 1.0/(1<<level)
    print "H =",H
    # generate matrices
    print "generate local to global map"
    Sigma = timefunc(pde.generate_local_global_map)(gv)
    print "generate cell/vertex info"
    cell_info = timefunc(pde.generate_vertex_cell_info)(grid,level)
    print "generate cell/cell info"
    coarse_cell_map = timefunc(pde.generate_cell_cell_info)(grid,level)
    #### Size info
    N_dofs = Sigma.shape[0]
    ####
    print "Generate dirichlet information"
    dirichletindicator = set_dirichlet(N,refine,[[1,0],[0,0]])
    # get coarse-to-fine basis mapping in coordinate format
    print "generate basis functions projection"
    val, row, col = pde.generate_basis_function_projection(grid,level)
    Psi = sparse.coo_matrix((val, (row, col))).tocsr()
    ####
    print "Generate Subdomain Info"
    SubInfo = restrictions.generate_subdomain_info(grid,level,dirichletindicator,Sigma,overlap,Psi)
    # write cell info
    for i,s in enumerate(SubInfo) :
        vtk = pde.VtkWriter(grid.leafGridView(),0)
        vtk.addCellData(1.0*coarse_cell_map, "Partition")
        vtk.addCellData(1.0*s._patchindicator
                        + 1.0*s._overlapindicator, "Overlap")
        vtk.addVertexData(1.0*s.R().transpose()*s._local_dirichlet,
                          "DirichletInfo")
        Lag = (Psi == 1.0) * 1.0
        vtk.addVertexData(Lag.transpose()*2.0*s._TH.transpose() * np.ones(s._TH.shape[0])
                          + Lag.transpose()*1.0*s._RH.transpose() * np.ones(s._RH.shape[0]),
                          "CoarseDOFInfo")
        print ">>> Write Cell VTK files"
        vtk.write("Cell-%i-%i-%i-%i" % (refine,level,overlapsize,i))

if __name__ == "__main__":
    # restrictions.__test__()
    run_test(N=[2,1], L=[2.0,1.0], refine=2, level=1, overlap=0.5, write_vtk=2)
