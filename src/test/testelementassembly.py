#!/usr/bin/python

import sys
import os
import site
site.addsitedir(os.path.dirname(sys.argv[0])+"/..")

import math
import time
import warnings
from IPython.terminal import embed
import cProfile
# import ipdb

# initialize MPI (mayhaps we link DUNE against MPI?!)
from mpi4py import MPI

# import numpy/scipy
import numpy as np
import scipy.sparse as sparse
from scipy.sparse import linalg as splinalg

# import DUNE
import lod
lod.time_measurement = True
from lod import timefunc
import pypdelab as pde

def test_element_assembly(refine = 1) :
    grid = pde.Grid([1,1],[1,1])
    model = pde.Problem9()
    grid.globalRefine(refine)
    gv = grid.leafGridView()

    #####
    print "Assemble matrix"
    A = pde.generate_stiffnessmatrix(gv, model)
    # b = pde.generate_rhs(gv, model)
    M = pde.generate_massmatrix(gv)

    #
    cells = (1<<refine)*(1<<refine);
    vertices = (1+(1<<refine))*(1+(1<<refine));
    print cells
    print vertices

    #####
    vertex_cell_info = pde.generate_vertex_cell_info(grid,grid.maxLevel())
    cell_cell_info = pde.generate_cell_cell_info(grid,refine-1)
    print vertex_cell_info
    print cell_cell_info

    #####
    Sigma = pde.generate_local_global_map(gv)
    print Sigma.shape
    assert (Sigma.shape == (vertices,cells*4))
    if vertices <= 10:
        print Sigma.todense()

    #####
    ME = pde.generate_element_massmatrix(gv)
    print ME.shape
    assert (ME.shape == (cells*4,cells*4))
    if vertices <= 10:
        print ME.todense()
    M2 = Sigma * ME * Sigma.transpose()
    print M2.shape
    assert (M2.shape == (vertices,vertices))
    if vertices <= 10:
        print M.todense()
        print M2.todense()
    print "M == M2", (not(M != M2).max())
    assert (not(M != M2).max())

if __name__ == "__main__":
    np.set_printoptions(linewidth=300,precision=2,threshold=4096)

    test_element_assembly(1)
    test_element_assembly(2)
    test_element_assembly(3)
