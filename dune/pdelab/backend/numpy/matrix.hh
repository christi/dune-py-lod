#ifndef NUMPY_MATRIX_HH
#define NUMPY_MATRIX_HH

#include <Python.h>
#include "vector.hh"

#include <boost/python/tuple.hpp>
#include <boost/python/import.hpp>

namespace NumPy {

    template<typename DA, typename IA>
    object createCSRMatrix(DA & data, IA & idx, IA & indptr)
    {
        object csr_args = py::make_tuple(
            vectorToPyObject(data),
            vectorToPyObject(idx),
            vectorToPyObject(indptr));

        boost::python::object scipy = py::import("scipy.sparse");
        py::object csr_matrix = scipy.attr("csr_matrix");
        return csr_matrix(csr_args);
    }

} // end namespace NumPy

#endif // NUMPY_MATRIX_HH
