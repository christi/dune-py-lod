import time
import cProfile
import numpy as np
from scipy import sparse

# a simple class to prepend text to each line of a given stream
class prependstream:
    def get_symbol(self,symbol,namespace):
        key = symbol[0]
        symbol = symbol[1:]
        if len(symbol) == 0:
            return namespace[key]
        else:
            return self.get_symbol(symbol,namespace[key].__dict__)
    def assign_to_symbol(self,obj,symbol,namespace):
        key = symbol[0]
        symbol = symbol[1:]
        if len(symbol) == 0:
            namespace[key] = obj
        else:
            namespace = namespace[key].__dict__
            self.assign_to_symbol(obj,symbol,namespace)
    def __init__(self,prepend,streamname,namespace = globals()):
        self.symbol = streamname.split(".")
        self.prepend = prepend
        self.namespace = namespace
        self.nextstream = self.get_symbol(self.symbol,self.namespace)
        self.line = ""
        self.assign_to_symbol(self,self.symbol,self.namespace)
    def detach(self):
        self.assign_to_symbol(self.nextstream,self.symbol,self.namespace)
    def __del__(self):
        self.detach()
    def write(self, string):
        self.line = self.line + string
        if (self.line[-1] == "\n"):
            self.nextstream.write(self.prepend + self.line)
            self.line = ""

# function decorator for timing operations
class timefunc(object):
    enable = False
    accumlated = 0.0
    def __init__(self,f):
        # keep a copy to avoid dangling pointers
        self.f = f
        # wrapper to compute the timings
        def _f_wrapper(*args, **kwargs):
            start = time.time()
            result = self.f(*args, **kwargs)
            end = time.time()
            if timefunc.enable:
                print ">>>", self.f.__name__, 'took', end - start, 'sec'
            timefunc.accumlated = timefunc.accumlated + (end - start)
            return result
        self.__name__="timefunc"
        self.__doc__=f.__doc__
        self.wrapper = _f_wrapper
    def __repr__(self):
        return "%s.%s wrapper of %s" % (self.__module__, self.__name__, repr(self.f))
    def __call__(self,*args, **kwargs):
        return self.wrapper(*args, **kwargs)
    @staticmethod
    def reset():
        timefunc.accumlated = 0.0

@timefunc
def generate_restrictions(cell_info,A,overlapsize) :
    # loop through patches
    cells = int(max(cell_info))+1
    N = len(cell_info)
    R_list = []
    INFO_CELL = -1
    # create matrix containing the pattern
    if overlapsize > 0:
        pattern = A.tocsr(copy=True)
        pattern.data = pattern.indices+.1 # we have to add a slight  shift to make sure that index 0 is not dropped from the pattern
    if A is not None:
        main = sparse.diags([A.diagonal()],[0],format="csr")
        main.data = main.indices+.1
    for c in range(cells):
        # extract fine mesh cell list
        col = np.where(cell_info == c)[0]
        # eliminate dirichlet nodes
        if A is not None:
            nz = main[col,:].data.astype(int)
            col = np.unique(nz)
        # extend with overlap size
        if c == INFO_CELL and overlapsize > 0:
            print col
        for i in range(overlapsize):
            if c == INFO_CELL and overlapsize > 0:
                for x in col:
                    print "NZ[" + str(x) + "] " + str(pattern[x,:].nonzero()[1])
                    print "NZ[" + str(x) + "] " + str((pattern[x,:].data).astype(int))
            nz = pattern[col,:].data.astype(int) # level 9/2: ca. 85sec
            # nz = A[col,:].nonzero()[1] # level 9/2: ca. 100 sec
            col = np.unique(nz)# .astype(int)
            if c == INFO_CELL:
                print "COL:" + str(col)
        col = np.unique(col)
        # create restriction matrix
        M = len(col)
        row = np.array(range(M), np.int)
        val = np.ones(M, np.int8)
        R = sparse.coo_matrix((val, (row, col)), (M,N))
        R_list.append(R)
    import sys
    if overlapsize > 0 and INFO_CELL >= 0:
        sys.exit(0)
    return R_list

@timefunc
def generate_coarse_restrictions(Rs,Psi) :
    RH_list = []
    for R in Rs:
        RH_list.append(generate_coarse_restriction(R,Psi))
    return RH_list

def diagonal_matrix(d):
    return sparse.dia_matrix((d,0), shape=(len(d),len(d)))

def eliminate_empty_rows(A):
    A = A.tocsr()
    indptr = np.unique(A.indptr)
    return sparse.csr_matrix((A.data,A.indices,indptr), shape=(len(indptr)-1,A.shape[1]))

def config_as_dict(config):
    if isinstance(config,basestring):
        return config
    the_dict = {}
    for k in config:
        the_dict[k] = config_as_dict(config[k])
    return the_dict

def imports(scope):
    for name, val in scope.items():
        if isinstance(val, types.ModuleType):
             yield val.__name__

def print_dependencies():
    import sys
    import importlib
    import types
    for mod in list(sys.modules.values()):
        # Some modules play games with sys.modules (e.g. email/__init__.py
        # in the standard library), and occasionally this can cause strange
        # failures in getattr.  Just ignore anything that's not an ordinary
        # module.
        if not isinstance(mod, types.ModuleType):
            continue
        if hasattr(mod,'__version__'):
            print "%s version %s" % (mod.__name__, str(mod.__version__))

class PVDWriter:
    def __init__(self):
        self.timesteps = []
    def addvtk(self, vtk):
        self.timesteps = self.timesteps + [vtk]
    def write(self, name):
        f = open(name,"w")
        f.write('''<?xml version="1.0"?>
<VTKFile type="Collection" version="0.1">
  <Collection>''')
        for i,v in enumerate(self.timesteps):
            f.write('\t<DataSet timestep="' + str(i) + '" group="" part="0" file="' + v + '"/>\n')
        f.write('''</Collection>
</VTKFile>''')
