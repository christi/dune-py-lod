#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>
#include <string>
#include <algorithm>
#include <iterator>
#include <fstream>

#include <dune/common/version.hh>
#include <dune/common/math.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/common/gridinfo.hh>

#include <dune/pdelab/function/const.hh>
#include <dune/pdelab/common/function.hh>
#include <dune/pdelab/localoperator/l2.hh>

#include <dune/pdelab/elementmatrixadapter.hh>

#include <boost/python.hpp>
namespace py = boost::python;

//===============================================================
// General configuration
//===============================================================
static const int dim = 3;
typedef Dune::YaspGrid<dim> BaseGrid;

#include "assembler.hh"
#include "pygrid.hh"

namespace Implementation {

    struct MassMatrixSetupBase
    {
        typedef Dune::PDELab::NoConstraints Constraints;
        typedef Dune::PDELab::NoConstraintsParameters ConstraintsParameters;
        typedef Dune::PDELab::L2 LocalOperator;
        LocalOperator lop() const
        {
            const int integration_order = 4;
            return LocalOperator(integration_order);
        }
        ConstraintsParameters constraints_parameters() const
        {
            return ConstraintsParameters();
        }
        template<typename GV>
        Dune::PDELab::ConstGridFunction<GV,double>
        dirichlet_extension(const GV & gv)
        {
            return Dune::PDELab::ConstGridFunction<GV,double> (gv,0.0);
        }
    };

}

template<typename GV, typename FEM, typename P>
struct MassMatrixSetup : public Implementation::MassMatrixSetupBase {
    typedef Implementation::MassMatrixSetupBase::Constraints Constraints;
    typedef Implementation::MassMatrixSetupBase::LocalOperator LocalOperator;
};

template<typename GV, SpaceType ElementWise>
py::object generate_massmatrix (const GridView<GV> & gv)
{
    return generate_matrix<GV,ElementWise, MassMatrixSetup, void*>(gv, 0);
}

class LocalGlobalMapping :
    public Dune::PDELab::LocalOperatorDefaultFlags
{
public:
    // pattern assembly flags
    enum { doPatternVolume = true };
    // residual assembly flags
    enum { doAlphaVolume = true };

    // define sparsity pattern of operator representation
    template<typename LFSU, typename LFSV, typename LocalPattern>
    void pattern_volume (const LFSU& lfsu, const LFSV& lfsv,
        LocalPattern& pattern) const
    {
        assert(lfsv.size() == lfsu.size());
        for (size_t i=0; i<lfsv.size(); ++i)
            pattern.addLink(lfsv,i,lfsu,i);
    }

    // jacobian of volume term
    template<typename EG, typename LFSU, typename X, typename LFSV, typename M>
    void jacobian_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv,
        M & mat) const
    {
        assert(lfsv.size() == lfsu.size());
        // Switches between local and global interface
        for (size_t i=0; i<lfsv.size(); i++)
            mat.accumulate(lfsv,i,lfsu,i,1);
    }
};

template<typename GV>
py::object generate_local_global_map (const GridView<GV> & wrapped_gv)
{
    GV gv = wrapped_gv.impl();

    // make finite element map
    typedef typename GV::Grid::ctype DF;
    typedef typename ::FEM<GV,DF,Conforming>::type FEM;
    FEM fem = ::FEM<GV,DF,Conforming>::instance(gv);
    typedef typename ::FEM<GV,DF,Discontinuous>::type DGFEM;
    DGFEM dgfem = ::FEM<GV,DF,Discontinuous>::instance(gv);

    // constants and types
    typedef typename FEM::Traits::FiniteElementType::Traits::
        LocalBasisType::Traits::RangeFieldType R;

    // make function space
    typedef Dune::PDELab::GridFunctionSpace<
        GV,
        FEM,
        Dune::PDELab::NoConstraints,
        Dune::PDELab::NumPyVectorBackend
        > GFS;
    GFS gfs(gv,fem);
    gfs.name("q1");
    gfs.update();

    // make dg function space
    typedef Dune::PDELab::GridFunctionSpace<
        GV,
        DGFEM,
        Dune::PDELab::NoConstraints,
        Dune::PDELab::NumPyVectorBackend
        > DGGFS;
    DGGFS dggfs(gv,dgfem);
    dggfs.name("element-wise");
    dggfs.update();

    // make local operator
    typedef LocalGlobalMapping LOP;
    LOP lop;

    // make grid operator
    typedef Dune::PDELab::GridOperator<DGGFS,GFS,LOP,
                                       Dune::PDELab::NumPyMatrixBackend,
                                       double,double,double
                                       > GridOperator;
    GridOperator gridoperator(dggfs,gfs,lop);

    // represent operator as a matrix
    typedef typename GridOperator::Traits::Jacobian M;
    M m(gridoperator, 1.0);

    // return matrix
    return Dune::PDELab::NumPy::getPyObject(m);
}

template<typename GV, template<typename,typename,typename> class TSetup, typename Problem>
py::object generate_dirichletinformation (const GridView<GV> & wrapped_gv, const Problem & problem)
{
    GV gv = wrapped_gv.impl();

    std::cout << "GV " << &gv << std::endl;
    std::cout << "PROBLEM " << &problem << std::endl;
    // make finite element map
    typedef typename GV::Grid::ctype DF;
    typedef Dune::PDELab::QkLocalFiniteElementMap<GV,DF,double,1> FEM;
    FEM fem(gv);

    // make matrix setup descrition
    typedef TSetup<GV, FEM, Problem> Setup;
    Setup setup;

    // constants and types
    typedef typename FEM::Traits::FiniteElementType::Traits::
        LocalBasisType::Traits::RangeFieldType R;
    typedef Dune::PDELab::ConformingDirichletConstraints CON;

    // make function space
    typedef Dune::PDELab::GridFunctionSpace<
        GV,
        FEM,
        CON,
        Dune::PDELab::NumPyVectorBackend
        > GFS;
    GFS gfs(gv,fem);
    gfs.name("solution");
    gfs.update();

    // make constraints map and initialize it from a function
    typedef typename GFS::template ConstraintsContainer<R>::Type C;
    C cg;
    cg.clear();
    Dune::PDELab::constraints(setup.constraints_parameters(),gfs,cg);

    // make coefficent Vector and initialize it from a function
    // typedef typename Dune::PDELab::BackendVectorSelector<GFS,uint8_t>::Type DV;
    // typedef typename Dune::PDELab::BackendVectorSelector<GFS,NumPy::Bool>::Type DV;
#if DUNE_VERSION_NEWER(DUNE_PDELAB,2,4)
    using DV = Dune::PDELab::Backend::Vector<GFS,double>;
#else
    typedef typename Dune::PDELab::BackendVectorSelector<GFS,double>::Type DV;
#endif
    DV x0(gfs,false);
    Dune::PDELab::set_constrained_dofs(cg,true,x0);

    // return bool vector
    return Dune::PDELab::NumPy::getPyObject(x0);
}

#include "pymappings.hh"
#include "pygrid.hh"

#include <dune/grid/yaspgrid.hh>
#include <dune/pdelab/localoperator/convectiondiffusionfem.hh>
#include "spe10.hh"

const bool spe10::pure_neumann;
const bool spe10::has_exact_solution;
const bool spe10dirichlet::pure_neumann;
const bool spe10dirichlet::has_exact_solution;
const double* spe10IO::permeability_ = nullptr;

template<typename GV, typename FEM, typename P>
struct PoissonSetup
{
private:
    typedef P Problem;
    typedef typename FEM::Traits::FiniteElementType::Traits::
        LocalBasisType::Traits::RangeFieldType R;
    typedef typename Problem::template Parameters<GV,R> LOPParam;
public:
    typedef Dune::PDELab::ConformingDirichletConstraints Constraints;
    typedef Dune::PDELab::ConvectionDiffusionBoundaryConditionAdapter<LOPParam> ConstraintsParameters;
    typedef Dune::PDELab::ConvectionDiffusionDirichletExtensionAdapter<LOPParam> DirichletExtension;
    typedef Dune::PDELab::ConvectionDiffusionFEM<LOPParam,FEM> LocalOperator;
    LocalOperator lop()
    {
        return LocalOperator(lopparam); // why a mutable reference?
    }
    ConstraintsParameters constraints_parameters() const
    {
        return ConstraintsParameters(lopparam);
    }
    DirichletExtension dirichlet_extension(const GV & gv)
    {
        return DirichletExtension(gv,lopparam); // why a mutable reference?
    }
private:
    LOPParam lopparam;
};

BOOST_PYTHON_MODULE(spe10)
{
    // initialize
    import_array();
    Dune::PDELab::NumPy::initialize();

    // register classes and functions
    import_grid();

    // mass matrix assembly
    py::def("generate_massmatrix", generate_massmatrix<Grid::Impl::LeafGridView, Conforming>,
        "store mass matrix in a scipy matrix.");
    py::def("generate_element_massmatrix", generate_massmatrix<Grid::Impl::LeafGridView, Discontinuous>,
        "store mass matrix in a numpy vector.");

    // projection operators
    py::def("generate_vertex_cell_info", generate_vertex_cell_info<Grid>,
        "generate a vector, which stores the mapping from levelGridView coarseLevel cells to leafGridView vertices.",
        (py::arg("grid"), py::arg("coarseLevel") = 0));
    py::def("generate_cell_cell_info", generate_cell_cell_info<Grid>,
        "generate a vector, which stores the mapping from levelGridView coarseLevel cells to leafGridView cells.",
        (py::arg("grid"), py::arg("coarseLevel") = 0));
    py::def("generate_local_global_map", generate_local_global_map<Grid::Impl::LeafGridView>,
        "generate a vector, which stores the mapping from leafGridView cell-vertices to leafGridView vertices (Sigma).");
    py::def("generate_basis_function_projection", generate_basis_function_projection<Grid>,
        "generate a matrix, which stores the representation of levelGridView coarseLevel basis functions in the leafGridView basis (Psi).",
        (py::arg("grid"), py::arg("coarseLevel") = 0));

    // model descriptors
    py::class_<spe10>(spe10::name().c_str())
        .def_readonly("pure_neumann", &spe10::pure_neumann)
        .def_readonly("has_exact_solution", &spe10::has_exact_solution)
        .def("name", &spe10::name)
        .staticmethod("name");
    // assembly operators
    py::def("generate_stiffnessmatrix", generate_matrix<Grid::Impl::LeafGridView, Conforming, PoissonSetup, spe10>,
        "store stiffness matrix in a scipy matrix.");
    py::def("generate_element_stiffnessmatrix", generate_matrix<Grid::Impl::LeafGridView, Discontinuous, PoissonSetup, spe10>,
        "store element operator in numpy vector.");
    py::def("generate_dirichletinformation", generate_dirichletinformation<Grid::Impl::LeafGridView, PoissonSetup, spe10>,
        "store a bool flag in a numpy vector. The value is true if a vertex is a Dirichlet vertex.");
    py::def("generate_element_rhs", generate_rhs<Grid::Impl::LeafGridView, Discontinuous, spe10>,
        "generate rhs for the poisson problem, store in a numpy vector.");

    py::class_<spe10dirichlet>(spe10dirichlet::name().c_str())
        .def_readonly("pure_neumann", &spe10dirichlet::pure_neumann)
        .def_readonly("has_exact_solution", &spe10dirichlet::has_exact_solution)
        .def("name", &spe10dirichlet::name)
        .staticmethod("name");
    // assembly operators
    py::def("generate_stiffnessmatrix", generate_matrix<Grid::Impl::LeafGridView, Conforming, PoissonSetup, spe10dirichlet>,
        "store stiffness matrix in a scipy matrix.");
    py::def("generate_element_stiffnessmatrix", generate_matrix<Grid::Impl::LeafGridView, Discontinuous, PoissonSetup, spe10dirichlet>,
        "store element operator in numpy vector.");
    py::def("generate_dirichletinformation", generate_dirichletinformation<Grid::Impl::LeafGridView, PoissonSetup, spe10dirichlet>,
        "store a bool flag in a numpy vector. The value is true if a vertex is a Dirichlet vertex.");
    py::def("generate_element_rhs", generate_rhs<Grid::Impl::LeafGridView, Discontinuous, spe10dirichlet>,
        "generate rhs for the poisson problem, store in a numpy vector.");

    // map exceptions from dune to python
    py::register_exception_translator<Dune::Exception>([] (const Dune::Exception & e) { PyErr_SetString(PyExc_RuntimeError, std::string(e.what()).c_str()); });
}
