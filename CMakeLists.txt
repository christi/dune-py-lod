cmake_minimum_required(VERSION 2.8.6)
project(dune-py-lod CXX)

if(NOT (dune-common_DIR OR dune-common_ROOT OR
      "${CMAKE_PREFIX_PATH}" MATCHES ".*dune-common.*"))
    string(REPLACE  ${CMAKE_PROJECT_NAME} dune-common dune-common_DIR
      ${PROJECT_BINARY_DIR})
endif()

#find dune-common and set the module path
find_package(dune-common REQUIRED)
list(APPEND CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake/modules"
  ${dune-common_MODULE_PATH})

#include the dune macros
include(DuneMacros)

# start a dune project with information from dune.module
dune_project()

add_subdirectory("dune")
add_subdirectory("doc")
add_subdirectory("lib")
add_subdirectory("src")
add_subdirectory("cmake/modules")

# find python
find_package(PythonInterp REQUIRED)
find_package(PythonLibs REQUIRED)
# find boost-python
# find_package(Boost 1.45.0 COMPONENTS python REQUIRED)
find_package(Boost COMPONENTS python REQUIRED)
message(STATUS "boost-python: ${CMAKE_BUILD_TYPE}")
# set(Boost_PYTHON_LIBRARY "${Boost_PYTHON_LIBRARY_DEBUG}" CACHE FILEPATH "Boost python library")

# write python path file
configure_file( module.pth.in module.pth )

# finalize the dune project, e.g. generating config.h etc.
finalize_dune_project(GENERATE_CONFIG_H_CMAKE)
