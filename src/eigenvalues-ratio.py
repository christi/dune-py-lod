#!/usr/bin/env python

import sys
import os
import site
site.addsitedir(os.path.dirname(sys.argv[0])+"/..")

import math
import warnings

# initialize MPI (mayhaps we link DUNE against MPI?!)
from mpi4py import MPI

# import numpy/scipy
import numpy as np
import math
import scipy
import scipy.sparse as sparse

# import DUNE
import pypdelab as pde
import lod
from lod import restrictions
from lod import timefunc
from lod import utils
from lod.linalg import sort_eig

# call iterative EV solver with CG/AMG solver
def solve_ev_problem(A, M, K):
    from pyamg import smoothed_aggregation_solver
    ml = timefunc(smoothed_aggregation_solver)(A)
    cg = lambda b : scipy.sparse.linalg.cg(A, b, M=ml.aspreconditioner())[0]
    Ainv = scipy.sparse.linalg.LinearOperator(shape=A.shape, matvec=cg)
    X = scipy.rand(A.shape[0], K)
    timefunc.reset()
    v0 = np.ones(A.shape[0])
    (ev,ef) = timefunc(scipy.sparse.linalg.eigsh)(scipy.sparse.linalg.aslinearoperator(A),
                                                  k=K,M=M,
                                                  OPinv=Ainv,
                                                  v0=v0,
                                                  sigma=0.0,which='LM',return_eigenvectors=True)
    return ev,ef,timefunc.accumlated

# import matplotlib
import pandas as pd
import matplotlib
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
plotdata = pd.DataFrame(columns=list(['L','l','H','h','overlap']))

# cache full EV results
full_ev_cache = {}

@timefunc
def run_test(basename,N,L,refine,level,overlap,write_vtk,model,verbose,ratio):
    K = 20 # number of eigenvalues we want to compute
    overlap = overlap * 1.0
    overlapsize = int((1<<(refine-level))*overlap)    # overlap for patch
    name = "%s-%i-%i-%i-%e" % (basename,refine,level,overlap*10,ratio)
    utils.prependstream(name+":\t", "sys.stdout", globals())
    H = (math.pow(0.5, level)*np.array(L)/np.array(N))
    # store some configuration options
    plotdata.loc[name,'L'] = level
    plotdata.loc[name,'l'] = refine
    plotdata.loc[name,'H'] = H[0]
    plotdata.loc[name,'h'] = math.pow(0.5, refine)*L[0]/N[0]
    plotdata.loc[name,'overlap'] = overlap
    plotdata.loc[name,'ratio'] = ratio
    # create grid and grid views
    print "create Grid"
    grid = pde.Grid(L,N)
    for i in range(refine):
        grid.globalRefine()
    gv = grid.leafGridView()
    coarse_gv = grid.levelGridView(level)
    dim = coarse_gv.dimension()
    print "generate local to global map"
    Sigma = timefunc(pde.generate_local_global_map)(gv)
    # generate sub domain info
    print "Mesh width:", H
    coarse_positions = coarse_gv.cellpositions()
    coarse_positions = coarse_positions.reshape((coarse_positions.shape[0]/dim,dim))
    fine_positions = gv.cellpositions()
    fine_positions = fine_positions.reshape((fine_positions.shape[0]/dim,dim))
    N_patches = coarse_positions.shape[0]
    print "generate fine mesh Dirichlet boundary information"
    dirichletindicator = pde.generate_dirichletinformation(gv,model)
    N_dofs = len(dirichletindicator)
    print "generate cell/cell info"
    coarse_cell_map = pde.generate_cell_cell_info(grid,level)
    ######################################################
    # get coarse-to-fine basis mapping in coordinate format
    print "generate basis functions projection"
    val, row, col = pde.generate_basis_function_projection(grid,level)
    Psi = sparse.coo_matrix((val, (row, col)))
    val, row, col = (None, None, None)
    ######################################################
    # generate subdomain info
    SubInfo = restrictions.generate_subdomain_info(grid,level,dirichletindicator,Sigma,H,overlap,Psi)
    # generate matrices
    print "generate element wise stiffness matrix"
    AE = timefunc(pde.generate_element_stiffnessmatrix)(gv,model)
    bE = np.zeros(AE.shape[0]) # we don't have a rhs, thus we will it with dummy data
    print "generate element wise massmatrix"
    ME = timefunc(pde.generate_element_massmatrix)(gv)
    print "generate global matrizes"
    M = Sigma * ME * Sigma.transpose()
    A = Sigma * AE * Sigma.transpose()
    ######################################################
    print "generate fine mesh Dirichlet information"
    Bh = utils.diagonal_matrix( np.logical_not(pde.generate_dirichletinformation(gv,model)) )
    # lagrange interpolation operator from fine space into coarse space
    print "generate Lagrange restriction operator"
    Lag = (Psi == 1.0) * 1.0
    print "generate coarse mesh Dirichlet information"
    BH = Lag * Bh * Lag.transpose()
    # coarse problem
    print "Solve coarse EV problem"
    BC = utils.eliminate_empty_rows(BH)
    (ev_coarse,ef_coarse,t_coarse) = solve_ev_problem(BC*Psi*A*Psi.transpose()*BC.transpose(),BC*Psi*M*Psi.transpose()*BC.transpose(), K)
    plotdata.loc[name,'t_coarse'] = t_coarse
    ######################################################
    # construct coarse space correction
    print "generate corrector functions"
    (Q_Psi,W) = lod.compute_correction(AE,bE,BH,M,Psi,Sigma,SubInfo,verbose=0,print_kernel=False)
    plotdata.loc[name,'t_corr'] = timefunc.accumlated
    # restrict to coarse space
    P = Psi + Q_Psi
    # LOD problem
    print "Solve LOD EV problem"
    (ev_lod,ef_lod,t_lod) = solve_ev_problem(BC*P*A*P.transpose()*BC.transpose(),BC*P*M*P.transpose()*BC.transpose(), K)
    plotdata.loc[name,'t_LOD'] = t_lod
    # full problem
    # we might skip the full computation...
    if True: # write_vtk or not( refine in full_ev_cache.keys() ):
        print "Solve full EV problem"
        BCh = utils.eliminate_empty_rows(Bh)
        Ah = BCh * A * BCh.transpose()
        Mh = BCh * M * BCh.transpose()
        (ev_full,ef_full, t_full) = solve_ev_problem(Ah,Mh, K)
        plotdata.loc[name,'t_full'] = t_full
        full_ev_cache[refine] = (t_full, ev_full)
    else:
        plotdata.loc[name,'t_full'] = full_ev_cache[refine][0]
        ev_full = full_ev_cache[refine][1]
    # print out EV results
    ev_coarse_err = np.abs(ev_coarse[:K]-ev_full)/np.linalg.norm(ev_full)
    ev_lod_err = np.abs(ev_lod[:K]-ev_full)/np.linalg.norm(ev_full)
    print "i,ev_full,ev_lod,ev_lod_err,ev_coarse,ev_coarse_err"
    for i in range(K):
        print i,ev_full[i],ev_lod[i],ev_lod_err[i],ev_coarse[i],ev_coarse_err[i]
    print "error_rel(ev_coarse) = %e" % ev_coarse_err.max()
    print "error_rel(ev_lod) = %e" % ev_lod_err.max()
    plotdata.loc[name,'err_coarse'] = ev_coarse_err.max()
    plotdata.loc[name,'err_LOD'] = ev_lod_err.max()
    # write VTK data
    if write_vtk > 0:
        for i in range(K):
            vtk = pde.VtkWriter(grid.leafGridView(),0)
            vtk.addCellData(1.0*coarse_cell_map,"coarse_cell")
            # ensure a given orientation, as the eigenvector is only determined up to a factor of +1/-1
            # we do so to keep the different eigenvectors comparable
            factor = 1.0;
            if ef_coarse[1,i] < 0.0:
                factor = -1.0
            vtk.addVertexData(factor * Psi.transpose()*BC.transpose()*ef_coarse[:,i],"ef_coarse")
            if ef_lod[1,i] < 0.0:
                factor = -1.0
            vtk.addVertexData(factor * P.transpose()*BC.transpose()*ef_lod[:,i],"ef_lod")
            if ef_full[1,i] < 0.0:
                factor = -1.0
            vtk.addVertexData(factor * BCh.transpose() * ef_full[:,i],"ef_full")
            print ">>> Write VTK output %s-%03d.vtu"%(name,i)
            vtk.write("%s-%03d"%(name,i))
    print plotdata
    # we modified sys.stdout, bring it back to default state
    annotation.detach()

def run_tests(fine_level, level, overlap):
    def exclude(v,l):
        l.remove(v)
        return l
    timefunc.enable = True
    fname = "tmp-%s-%i-%i.csv" % (basename,fine_level,overlap)
    for ratio in [2e2,2e3,2e4,2e5,2e6]:
        print "SimplifiedKronigPenney model, ratio = %e" % ratio
        model = pde.SimplifiedKronigPenney(20,ratio)
        run_test(basename, N=[2,3], L=[2,3], refine=fine_level, level=level, overlap=overlap, model=model, write_vtk=0, verbose=0, ratio=ratio)
        plotdata.to_csv(fname)

def generate_plots(basename, fine_level, level, overlap, show = False):
    if not show:
        print "do not show output"
        matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    f = plt.figure()
    name = "%s-%i-%i" % (basename,fine_level,overlap)
    ## plot 1
    ax1 = plotdata.plot(x='ratio',y='err_coarse_p',label='$err_{coarse,H}$',color='#FF7F0E',marker='.',ax=f.gca(),legend=True)
    plotdata.plot(x='ratio',y='err_LOD_p',label='$err_{LOD,H}$',color='#1F77B4',marker='.',ax=ax1,legend=True)
    ax1.xaxis.set_ticks([1.0,1.5,2.0,2.5,3.0])
    ax2 = plotdata.plot(x='ratio',y='t_coarse',label='$t_{coarse,H}$',color='#FF7F0E',marker='x',ls='--',ax=ax1,secondary_y=True,legend=True)
    plotdata.plot(x='ratio',y='t_LOD',label="$t_{LOD,H}$",color='#1F77B4',ax=ax1,marker='x',ls='--',secondary_y=True,legend=True)
    plotdata.plot(x='ratio',y='t_corr',label='$t_{corr}$',color='#D62728',marker='x',ls='--',ax=ax1,secondary_y=True,legend=True)
    plotdata.plot(x='ratio',y='t_full',label='$t_{fine}$',color='#2CA02C',marker='x',ls='--',ax=ax1,secondary_y=True,legend=True)
    ax1.set_ylabel('relative error [%]')
    ax2.set_ylabel('computation time [s]')
    ax1.semilogx()
    ax1.semilogy()
    ax2.semilogx()
    ax2.semilogy()
    handles1, labels1 = ax1.get_legend_handles_labels()
    handles2, labels2 = ax2.get_legend_handles_labels()
    lgd1 = ax1.legend(handles = handles1, loc=2)
    lgd2 = ax2.legend(handles = handles2, loc=1)
    f.savefig("%s-ratio.pdf" % name, bbox_extra_artists=(lgd1,lgd2,), bbox_inches='tight')
    if show:
        plt.show()

if __name__ == "__main__":
    np.set_printoptions(threshold=np.inf)
    basename = "Eigenvalues-AMG-RATIO"
    fine_level = 6
    level = 3
    overlap = 1.0
    fname = "%s-%i-%i.csv" % (basename,fine_level,overlap)
    if not os.path.isfile(fname):
        run_tests(fine_level,level,overlap)
        plotdata.sort_values(by=['L','l','overlap'], inplace=True)
        plotdata.to_csv(fname)
    else:
        plotdata = pd.read_csv(fname, index_col=0)
    plotdata['t_LOD_all'] = plotdata.t_LOD + plotdata.t_corr
    plotdata['err_LOD_p'] = plotdata.err_LOD * 100
    plotdata['err_coarse_p'] = plotdata.err_coarse * 100
    generate_plots(basename, fine_level, level, overlap=1.0, show=False)
    print plotdata
