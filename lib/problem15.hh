struct Problem15
{
    static const bool has_exact_solution = false;
    static const bool pure_neumann = true;
    static const std::string name() { return "Problem15"; }

    template<typename GV, typename RF>
    class Parameters
    {
        typedef Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;

    public:
        typedef Dune::PDELab::ConvectionDiffusionParameterTraits<GV,RF> Traits;

        //! tensor diffusion coefficient
        typename Traits::PermTensorType
        A (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
        {
            typename Traits::DomainType x = e.geometry().global(xlocal);

            double coefficient = 0.0;
            bool in_isolator = false;

            if ( (x[1] >= 0.05) && (x[0]<= 0.95) )
            {
                double center[2];
                center[0] = 0.95;
                center[1] = 0.05;

                double radius = 0.9;

                double distance = sqrt( pow( center[0] - x[0], 2.0 ) + pow( center[1] - x[1], 2.0 ) );

                if ( std::fabs( distance - radius ) < 0.025 )
                {
                    coefficient = 1e-3;
                    in_isolator = true;
                }
            }

            if ( !in_isolator )
            {
                double eps = 0.05; /////////// EPSILON //////////

                coefficient = 10.0;

                for ( double j = 0.0; j < 5.0; j++ )
                    for ( double i = 0.0; i < j+1; i++ )
                    { coefficient += (2.0 / (j + 1.0) ) * ( cos( std::floor( (i*x[1]) - (1.0/(1.0+i))*x[0] ) + std::floor( i*x[0] / eps) + std::floor(x[1] / eps) ) ); }
                coefficient /= 10.0;

                if ( (coefficient < 1.0) && ( coefficient > 0.5 ) )
                    coefficient = pow( coefficient, 4.0 );

                if ( (coefficient > 1.0) && ( coefficient < 1.5 ) )
                    coefficient = pow( coefficient, 1.5 );
            }

            return {{coefficient, 0.0},{0.0, coefficient}};
        }

        //! velocity field
        typename Traits::RangeType
        b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
        {
            typename Traits::RangeType v(0.0);
            return v;
        }

        //! sink term
        typename Traits::RangeFieldType
        c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
        {
            return 0.0;
        }

        //! source term
        typename Traits::RangeFieldType
        __attribute__((hot))
        f (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
        {
            typename Traits::DomainType x = e.geometry().global(xlocal);
            return x[0] - 0.5;
        }

        //! boundary condition type function
        BCType
        bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
        {
            return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Neumann;
        }

        //! Dirichlet boundary condition value
        typename Traits::RangeFieldType
        g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
        {
            // assert(false && "wrong boundary condition");
            return 0.0;
        }

        //! Neumann boundary condition
        typename Traits::RangeFieldType
        j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
        {
            return 0.0;
        }

        //! outflow boundary condition
        typename Traits::RangeFieldType
        o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
        {
            assert(false && "wrong boundary condition");
            return 0.0;
        }
    };

}; // end Problem15
