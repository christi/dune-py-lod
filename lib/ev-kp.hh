struct SimplifiedKronigPenney
{
    static const bool has_exact_solution = false;
    static const bool pure_neumann = false;
    static const std::string name() { return "SimplifiedKronigPenney"; }

    static double k;
    static double ratio;

    SimplifiedKronigPenney(double _k = 16.0, double _ratio = 1e3)
    {
        k = _k;
        ratio = _ratio;
    }

    template<typename GV, typename RF>
    class Parameters
    {
        typedef Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;

    public:
        typedef Dune::PDELab::ConvectionDiffusionParameterTraits<GV,RF> Traits;

        //! tensor diffusion coefficient
        typename Traits::PermTensorType
        A (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
        {
            return {{1.0, 0.0},{0.0, 1.0}};
        }

        //! velocity field
        typename Traits::RangeType
        b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
        {
            typename Traits::RangeType v(0.0);
            return v;
        }

        //! sink term
        typename Traits::RangeFieldType
        __attribute__((hot))
        c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
        {
            auto xglobal = e.geometry().global(x);
#if 1
            // if (xglobal[0]*k <= 2.0 or xglobal[1]*k <= 4.0)
            //     return 1e3;

            const RF Pi = Dune::MathematicalConstants<RF>::pi();
            const RF v = cos( Pi * k * (xglobal[0]+0.1) ) * cos( Pi * k * xglobal[1] );

            // return (1.0 + v)*1e3;
            return std::ceil(v)*ratio;
#else
            const RF width=1.0/k;
            int ix,iy,iz;

            ix=((int)floor(xglobal[0]/width))%2;
            iy=((int)floor(xglobal[1]/width))%2;
            if (GV::dimension>2)
                iz=((int)floor(xglobal[2]/width))%2;
            else
                iz=0;

            RF k;

            RF K000=20.0;
            RF K001=0.002;
            RF K010=0.2;
            RF K011=2000.0;
            RF K100=1000.0;
            RF K101=0.001;
            RF K110=0.1;
            RF K111=10.0;

            if ( iz==0 && iy==0 && ix==0 ) k=K000;
            if ( iz==0 && iy==0 && ix==1 ) k=K001;
            if ( iz==0 && iy==1 && ix==0 ) k=K010;
            if ( iz==0 && iy==1 && ix==1 ) k=K011;
            if ( iz==1 && iy==0 && ix==0 ) k=K100;
            if ( iz==1 && iy==0 && ix==1 ) k=K101;
            if ( iz==1 && iy==1 && ix==0 ) k=K110;
            if ( iz==1 && iy==1 && ix==1 ) k=K111;
            return -k;
#endif
        }

        //! source term
        typename Traits::RangeFieldType
        f (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
        {
            return 0.0;
        }

        //! boundary condition type function
        BCType
        bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
        {
            return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
        }

        //! Dirichlet boundary condition value
        typename Traits::RangeFieldType
        g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
        {
            return 0.0;
        }

        //! Neumann boundary condition
        typename Traits::RangeFieldType
        j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
        {
            assert(false && "wrong boundary condition");
            return 0.0;
        }

        //! outflow boundary condition
        typename Traits::RangeFieldType
        o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
        {
            assert(false && "wrong boundary condition");
            return 0.0;
        }
    };

}; // end SimplifiedKronigPenney
