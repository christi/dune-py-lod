# setup copy commands for python files
set(PYFILES
  testnumpybackend.py
  testelementassembly.py
  testgrid.py
  testrestrictions.py
  )
dune_symlink_to_source_files(FILES ${PYFILES})
