py-lod
======

This package provides a prototype implementation of the
*local orthogonal decomposition* method.
It is intended as an example code, accompanying the paper
*Efficient implementation of the Localized Orthogonal Decomposition
method*, C. Engwer, P. Henning, A. Målqvist, and D. Peterseim.

python requirements
-------------------

 * pip install numpy scipy pyamg mpi4py pandas matplotlib

DUNE requirement
----------------

DUNE is required to assmble the fine scale problem and the different
helper matrices. The following DUNE modules are required

 * DUNE core modules (version 2.4.1)
 * dune-pdelab (version 2.4.1)

Installation
------------

Please follow the installation guidelines of DUNE
(see https://dune-project.org/doc/installation/).

Runnig the examples
-------------------

We provide several simple examples. In particular this code allows to
reproduce the results from the above mentioned paper.

  1. `src/eigenvalues.py` runs the test of the eigenvalue problem in
     section 7 of the paper. We consider a model problem inspired by
     the Kronig-Penney problem in quantum mechanics.
  2. `src/pypoisson.py` allows running simple example problems for the
     LOD, some of the setups were published in previous LOD
     papers. These are only single run forward models.
